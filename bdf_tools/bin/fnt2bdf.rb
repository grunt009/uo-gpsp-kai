#! /usr/bin/ruby
# fnt2bdf.rb ver 2.0
# Rockboxのfntファイルをbdfファイルに変換
#
# 使い方
# fnt2bdf.rb fnt_file
# BDFファイルは標準出力に出力
# ヘッダー部は最低限の出力のみ

# header の読込み
def read_header(file)
  header = { "version"     => nil, \
             "maxwidth"    => 0, \
             "height"      => 0, \
             "ascent"      => 0, \
             "pad"         => 0, \
             "firstchar"   => 0, \
             "defaultchar" => 0, \
             "size"        => 0, \
             "nbits"       => 0, \
             "noffset"     => 0, \
             "nwidth"      => 0 }
  file.pos = 0
  header['version'] = file.read(4).unpack("A4").pop
  header['maxwidth'] = file.read(2).unpack("S").pop.to_i
  header['height'] = file.read(2).unpack("S").pop.to_i
  header['ascent'] = file.read(2).unpack("S").pop.to_i
  header['pad'] = file.read(2).unpack("S").pop.to_i
  header['firstchar'] = file.read(4).unpack("I").pop.to_i
  header['defaultchar'] = file.read(4).unpack("I").pop.to_i
  header['size'] = file.read(4).unpack("I").pop.to_i
  header['nbits'] = file.read(4).unpack("I").pop.to_i
  header['noffset'] = file.read(4).unpack("I").pop.to_i
  header['nwidth'] = file.read(4).unpack("I").pop.to_i
#puts header
  return header
end

# offset, width の読込み
# offsetのサイズを返す
def read_offset_width(file, header)
  if header['nbits'] < 0xFFDB
    file.pos = 36 + (( header['nbits'] + 0x01 ) & 0xFFFFFFFE)
    offset = file.read(header['noffset'] * 2).unpack("S*")
    size = 2
  else
    file.pos = 36 + (( header['nbits'] + 0x03 ) & 0xFFFFFFFC)
    offset = file.read(header['noffset'] * 4).unpack("L*")
    size = 4
  end

  width = file.read(header['nwidth']).unpack("C*")

  return offset, width, size
end

# bitsの読み込み
def read_bits(file, bits, header, width, offset)
  flag = 0
  chars = 0
  default_offset = offset[header['defaultchar']]
  file.pos = 36
  for loop in header['firstchar'] .. (header['firstchar'] + header['size'] - 1)
    if ((offset[loop - header['firstchar']] == default_offset) && (flag == 1))
      bits[loop] = nil
    else
      w = width[loop - header['firstchar']]
      if (w == nil)
        w = header['maxwidth']
      end

      size = ( (header['height'] + 7) / 8 ) * w

      if (offset[loop - header['firstchar']] != nil)
        file.pos = 36 + offset[loop - header['firstchar']]
        flag = 1 if offset[loop - header['firstchar']] == default_offset
      else
        file.pos = 36 + (loop - header['firstchar']) * size
      end

        bits[loop] = file.read(size).unpack("C*")
        chars += 1
    end
  end
  return chars
end

# bdfヘッダの表示
def write_bdf_header(header, chars, max_bbx)
  STDOUT.puts "STARTFONT 2.1"
  STDOUT.puts "SIZE #{header['height']} 75 75"
#  STDOUT.puts "FONTBOUNDINGBOX #{header['maxwidth']} #{header['height']} 0 #{header['ascent'] - header['height']}"
  STDOUT.puts "FONTBOUNDINGBOX #{max_bbx[0]} #{max_bbx[1]} #{max_bbx[2]} #{max_bbx[3]}"
  STDOUT.puts "STARTPROPERTIES 4"
  STDOUT.puts "FONT_ASCENT #{header['ascent']}"
  STDOUT.puts "FONT_DESCENT #{header['height'] - header['ascent']}"
  STDOUT.puts "DEFAULT_CHAR #{header['defaultchar']}"
  STDOUT.puts "CHARSET_REGISTRY \"ISO10646\""
  STDOUT.puts "ENDPROPERTIES"
  STDOUT.puts "CHARS #{chars}"
end

# bdfデータへの変換
def conv_bdf(header, bits, width, chars)
  bdf = []
  bbx = []
  temp = []
  max_bbx = [0, 0, 0, 0]

  for loop in header['firstchar'] .. (header['firstchar'] + header['size'] - 1)
    if bits[loop] != nil
      num = 0

      char_y = header['height']
      bdf[loop] = []

      if (width[loop - header['firstchar']] != nil)
        char_x = width[loop - header['firstchar']]
        bbx[loop] = [width[loop - header['firstchar']], header['height'], 0, 0]
      else
        char_x = header['maxwidth']
        bbx[loop] = [header['maxwidth'], header['height'], 0, 0]
      end

      # ビットマップデータに一度変換
      for y in 0 .. (((char_y + 7) / 8) - 1)
        for bit in 0 .. 7
          for x in 0 .. (char_x - 1)
            temp[num] = bits[loop][x + (y * char_x)][bit]
            num += 1
          end
        end
      end

      # x位置の調整 (左)
      start_x = 0
      end_x = char_x - 1
      sum = 0
      x = 0
      while (sum == 0) && (x < char_x)
        sum = 0
        for y in 0 .. char_y - 1
          sum += temp[x + (y * char_x)].to_i
        end
        start_x += 1 if (sum == 0)
        x += 1
      end
      start_x = 0 if (sum == 0)

      # x位置の調整 (右)
      sum = 0
      x = end_x
      while (sum == 0) && (x > start_x - 1)
        sum = 0
        for y in 0 .. char_y - 1
          sum += temp[x + (y * char_x)].to_i
        end
        end_x -= 1 if (sum == 0)
        x -= 1
      end
      end_x = 0 if (sum == 0)

      bbx[loop][0] = end_x - start_x + 1
      bbx[loop][2] = start_x

      # y位置の調整 (上)
      start_y = 0
      end_y = char_y - 1
      sum = 0
      y = 0
      while (sum == 0) && (y < char_y)
        sum = 0
        for x in 0 .. char_x - 1
          sum += temp[x + (y * char_x)].to_i
        end
        start_y += 1 if sum == 0
        y += 1
      end
      start_y = 0 if sum == 0

      # y位置の調整 (下)
      sum = 0
      y = end_y
      while (sum == 0) && (y >= 0)
        sum = 0
        for x in 0 .. char_x - 1
          sum += temp[x + (y * char_x)].to_i
        end
        end_y -= 1 if sum == 0
        y -= 1
      end
      end_y = 0 if sum == 0

      bbx[loop][1] = end_y - start_y + 1
      bbx[loop][3] = header['ascent'] - end_y - 1
      bbx[loop][3] = 0 if (start_y == 0) && (end_y == 0) && (start_x == 0) && (end_x == 0)

      max_bbx[0] = max(max_bbx[0], bbx[loop][0])
      max_bbx[1] = max(max_bbx[1], bbx[loop][1])
      max_bbx[2] = min(max_bbx[2], bbx[loop][2])
      max_bbx[3] = min(max_bbx[3], bbx[loop][3])

     # bdfデータに変換
     for y in start_y .. end_y
       num = ""
       len = 0
       for x in start_x .. end_x
         num = num + temp[x + y * char_x].to_s(2)
         len += 1
       end
         num = num + "0" * ((8 - len) % 8)
       data = ("0b"+num).oct
       w = ((bbx[loop][0] + 7) / 8) * 2
       bdf[loop].push(format("%0#{w}X",data))
     end

   else
     bdf[loop] = nil
   end

   if((bdf[loop] == ["00"]) && (bbx[loop][0] == 1) && (bbx[loop][1] == 1) && (bbx[loop][2] == 0) && (bbx[loop][3] == 0) && (loop != header['defaultchar']))
     bdf[loop] = nil
     chars -= 1
   end

  end
  return bdf, bbx, max_bbx, chars
end

# bdfデータの表示
def write_bdf(header, bdf, width, bbx)
  for loop in header['firstchar'] .. (header['firstchar'] + header['size'] - 1)
   if bdf[loop] != nil
     STDOUT.puts "STARTCHAR #{loop}"
     STDOUT.puts "ENCODING #{loop}"

     if (width[loop - header['firstchar']] != nil)
       STDOUT.puts "DWIDTH #{width[loop - header['firstchar']]} 0"
     else
       STDOUT.puts "DWIDTH #{bbx[loop][0] + bbx[loop][2]} 0"
     end

     STDOUT.puts "BBX #{bbx[loop][0]} #{bbx[loop][1]} #{bbx[loop][2]} #{bbx[loop][3]}"
     STDOUT.puts "BITMAP"
     STDOUT.puts bdf[loop]
     STDOUT.puts "ENDCHAR"
   end
  end

  STDOUT.puts "ENDFONT"
end

# max
def max(a, b)
  if b > a
    return b
  else
    return a
  end
end

# min
def min(a, b)
  if a < b
    return a
  else
    return b
  end
end

# main
header = 0
bits = []
offset = 0
offset_size = 2
width = 0
chars = 0

bdf_data = []
bbx = []
max_bbx = []

file = open(ARGV[0])

header = read_header(file)
offset, width, offset_size = read_offset_width(file, header)
chars = read_bits(file, bits, header, width, offset)

file.close

bdf_data, bbx, max_bbx, chars = conv_bdf(header, bits, width, chars)

write_bdf_header(header, chars, max_bbx)
write_bdf(header, bdf_data, width, bbx)

exit
