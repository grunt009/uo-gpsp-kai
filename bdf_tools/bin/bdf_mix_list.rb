#! /usr/bin/ruby
#
# bdf_mix_list.rb ver 2.0
# listファイルを元にBDFファイルを合成します
#
# 使い方
# bdf_mix_list.rb list_file
#
# 以下のファイルが必要となります
# sbitget.exe / TTFDUMP.EXE / ttc2ttf.exe / TTF2BDF.EXE
# ttc2bdf.rb / ttf2bdf.rb / bdf_remap.rb / bdf_mix.rb
# その他 コマンド行に記載されているファイル等

#--------------------------------------------------------------------------
#定数の定義
#--------------------------------------------------------------------------
BDF_MIX = "./bin/bdf_mix.rb"
CP = "cp"

#--------------------------------------------------------------------------
#グローバル変数の定義
#--------------------------------------------------------------------------
$input_dir = nil
$mix_dir = nil
$out_dir = nil

#--------------------------------------------------------------------------
#コマンド
#--------------------------------------------------------------------------
def read_command(file)
  opt = ""
  n_opt = ""
  while line = file.gets
    case line
    #終了
    when /^\$/
      exit 0
    when /^#/
    when /^\s*$/
    #ディレクトリ
    when /^\*\s*(\S+)\s+(\S+)\s+(\S+)/
      $input_dir = $1
      $mix_dir = $2
      $out_dir = $3
    #コマンド
    when /\!\s+(.+)$/
      out = `#{$1}`
    when /^%\s+(.*)/
      opt = $1
      n_opt = $1
      puts opt
    when /^%%\s+(.*)/
      opt = $1
      n_opt = ""
    when /^\s*(\S+)\s+(\S+)\s+(\S+)\s*$/
      if $2 == '*'
        out = `#{CP} #{opt} #{$input_dir}/#{$1} #{$out_dir}/#{$3}`
#        STDERR.puts "#{CP} #{opt} #{$input_dir}/#{$1} #{$out_dir}/#{$3}"
      else
        out = `#{BDF_MIX} #{opt} #{$input_dir}/#{$1} #{$mix_dir}/#{$2} > #{$out_dir}/#{$3}`
#        STDERR.puts "#{BDF_MIX} #{opt} #{$input_dir}/#{$1} #{$mix_dir}/#{$2} > #{$out_dir}/#{$3}"
      end
      opt = n_opt
    when /^\s*(\S+)\s+(\S+)\s*$/
      if $2 == '*'
        out = `#{CP} #{opt} #{$input_dir}/#{$1} #{$out_dir}/#{$1}`
#        STDERR.puts "#{CP} #{opt} #{$input_dir}/#{$1} #{$out_dir}/#{$1}"
      else
        out = `#{BDF_MIX} #{opt} #{$input_dir}/#{$1} #{$mix_dir}/#{$2} > #{$out_dir}/#{$1}`
#        STDERR.puts "#{BDF_MIX} #{opt} #{$input_dir}/#{$1} #{$mix_dir}/#{$2} > #{$out_dir}/#{$1}"
      end
      opt = n_opt
    end
  end
end


#--------------------------------------------------------------------------
# 利用方法
#--------------------------------------------------------------------------
def usage
  STDERR.puts "usage: bdf_mix_list.rb list_file"
  exit 1
end

#--------------------------------------------------------------------------
# main
#--------------------------------------------------------------------------

# 引数なしの場合
if ARGV.size == 0
  usage
end

# ファイルのオープン
list_file = open(ARGV[0])

#コマンド
read_command(list_file)

# ファイルのクローズ
list_file.close

STDERR.puts "Complete."
exit
