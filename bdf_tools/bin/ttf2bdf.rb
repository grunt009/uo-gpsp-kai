#! /usr/bin/ruby
#
# ttf2bdf.rb ver 2.0
# TTFファイル内部のbitmapデータからBDFファイルを作成
#
# 使い方
# ttf2bdf.rb ttf_file font_name
# font_name1を指定するとBDFファイル名に使用されます
# ※指定が無い場合は内部名を利用します
#
# 作成されるファイル名は高さ+フォント名となります
#
# -b オプションを付けると、同時にフォント一覧のbmpファイルを作成します
# ※要 bdf2bmp.exe
#
# 以下のファイルが必要となります
# sbitget.exe / TTFDUMP.EXE / bdf_remap.rb / (bdf2bmp.exe)

#--------------------------------------------------------------------------
#定数の定義
#--------------------------------------------------------------------------
SBITGET = "./bin/sbitget.exe"
TTFDUMP = "./bin/TTFDUMP.EXE"
BDF_REMAP = "./bin/bdf_remap.rb"
BDF2BMP = "./bin/bdf2bmp.exe"

#--------------------------------------------------------------------------
# 利用方法
#--------------------------------------------------------------------------
def usage
  STDERR.puts "usage: ttf2bdf.rb [-b] ttf_file [font_name]"
  exit 1
end

#--------------------------------------------------------------------------
# main
#--------------------------------------------------------------------------
rename_flag = 0
bdf_num = 0
bdf_file = []
font_name = ""
font_h = []
make_bmp = 0

# 引数なしの場合
if ARGV.size == 0
  usage
end

if /-b/ =~ ARGV[0]
  make_bmp = 1
  ARGV.shift
end

#dumpデータの作成
`#{TTFDUMP} -tname -tcmap -tOS/2 -thmtx -nx #{ARGV[0]} > #{ARGV[0]}.dump`
STDERR.puts "Make dump file."

#ビットマップデータの作成
out = `#{SBITGET} #{ARGV[0]} 2>&1`

# TTFファイルの数とファイル名を取得
out.each do |line|
  if /wrote\s+'(.+)'/ =~ line
    bdf_file[bdf_num] = $1
#    STDERR.puts bdf_file[bdf_num]
    bdf_num += 1
  end
end
STDERR.puts "Read bdf file #{bdf_num}."


# TTFファイルが作成されなかった場合
if bdf_num == 0
  STDERR.puts "No BDF file"
  exit 1
end

# 基本フォント名を取得
if ARGV[1] != nil
  font_name = ARGV[1]
else
  /(.+)-\d+px.bdf/ =~ bdf_file[0]
  # ' 'は'_'に変換
  font_name = $1.gsub(/\s/, "_")
end

# 各BDFファイルの高さを取得
for i in 0 ... bdf_num
  /.+-(\d+)px.bdf/ =~ bdf_file[i]
  font_h[i] = $1
end

# 各BDFファイルをリネーム
for i in 0 ... bdf_num
  new_name = font_h[i] + "-" + font_name + ".bdf"
  File.rename(bdf_file[i], new_name + ".tmp")
  STDERR.puts "Make #{new_name}"
  bdf_file[i] = new_name
end

# 各BDFファイルを修正

# bdfデータの作成
for i in 0 ... bdf_num
  out = `#{BDF_REMAP} #{bdf_file[i] + ".tmp"} #{ARGV[0]}.dump > #{bdf_file[i]} 2>/dev/null`
  File.delete(bdf_file[i] + ".tmp")
  STDERR.puts "Remap #{bdf_file[i]}"
end

# bmpデータの作成
if make_bmp == 1
  for i in 0 ... bdf_num
    out = `#{BDF2BMP} -w #{bdf_file[i]} #{bdf_file[i].gsub(/.bdf/,".bmp")}`
  end
end

File.delete(ARGV[0] + ".dump")

exit
