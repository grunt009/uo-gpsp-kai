#! /usr/bin/ruby
#
# bdf_mix.rb ver 2.0
# 2種類のBDFファイルを合成
#
# 使い方
# bdf_mix.rb [-s option] [-b shift] bdf_file_1 bdf_file_2
# BDFファイルは標準出力に出力
# ヘッダー部は bdf_file_1の物を使用する
#
# FONTBOUNDINGBOXの1番/2番パラメータは大きい数値を使用する
#
# ENCODING番号が重複した場合は bdf_file_1のフォントを使用する
# ただし、オプション -2 が指定されていた場合は bdf_file_2のフォントを使用する

#--------------------------------------------------------------------------
#グローバル変数の定義
#--------------------------------------------------------------------------
$font_0 = []
$font_1 = []
$font_mix = []
$map = []
$shift_0 = []
$shift_1 = []

#--------------------------------------------------------------------------
#ヘッダー部分を読込み
#--------------------------------------------------------------------------
def read_head(file_0, file_1)
  a = 0; d = 0
  x1_0, y1_0, x2_0, y2_0 , a_0, d_0 = read_head_sub(file_0, 1)
  x1_1, y1_1, x2_1, y2_1 , a_1, d_1 = read_head_sub(file_1, 0)
  # 現状ではbdf_file_1のascent/descentをそのまま利用
  # Rockboxのフォントを日本語化するのが目的のため、高さが変わると問題があるため
  a = a_0
  d = d_0
  # SHIFT値はdescentの値を利用
  shift = d_0 - d_1
  return max(x1_0, x1_1), max(y1_0, y1_1), x2_0, y2_0, a, d, shift
end

#--------------------------------------------------------------------------
#ヘッダー部分を読込み sub
# FONTBOUNDINGBOXの手前までを読み込み
#--------------------------------------------------------------------------
def read_head_sub(file, out)
  x1 = 0; y1 = 0; x2 = 0; y2 = 0
  ascent = nil; descent = nil
  while line = file.gets
    case line
    # FONTBOUNDINGBOX
    when /FONTBOUNDINGBOX\s+(-*\d+)\s+(-*\d+)\s+(-*\d+)\s+(-*\d+)/
      x1 = $1.to_i; y1 = $2.to_i; x2 = $3.to_i; y2 = $4.to_i
    # FONT_ASCENT
    when /FONT_ASCENT\s+(-*\d+)/
      ascent = $1.to_i
    # FONT_DESCENT
    when /FONT_DESCENT\s+(-*\d+)/
      descent = $1.to_i
    # ENDPROPERTIES
    when /ENDPROPERTIES.*$/
      break
    end
  end
  # FONT_ASCENT/FONT_DESCENT が無い場合
  if ascent == nil
    ascent = y1 + y2
  end
  if descent == nil
    descent = y1 - ascent
  end
#  STDERR.puts x1, y1, x2, y2, ascent, descent
  return x1, y1, x2, y2, ascent, descent
end

#--------------------------------------------------------------------------
#--------------------------------------------------------------------------
def write_head(file, bx1, by1, bx2, by2, ascent, descent)
  flags = 0
  file.pos = 0
  while line = file.gets
    case line
    # FONTBOUNDINGBOX
    when /FONTBOUNDINGBOX\s+(-*\d+)\s+(-*\d+)\s+(-*\d+)\s+(-*\d+)/
      STDOUT.puts "FONTBOUNDINGBOX #{bx1} #{by1} #{bx2} #{by2}"
    # FONT_ASCENT
    when /FONT_ASCENT\s+(-*\d+)/
      STDOUT.puts "FONT_ASCENT #{ascent}"
      flags += 2
    # FONT_DESCENT
    when /FONT_DESCENT\s+(-*\d+)/
      STDOUT.puts "FONT_DESCENT #{descent}"
      flags += 1
    # ENDPROPERTIES
    when /ENDPROPERTIES.*$/
      break
    else
      STDOUT.print line
    end
  end

  # FONT_ASCENT/FONT_DESCENTが無い場合
  case flags
  when 0
    STDOUT.puts "FONT_ASCENT #{ascent}"
    STDOUT.puts "FONT_DESCENT #{descent}"
  when 1
    STDOUT.puts "FONT_ASCENT #{ascent}"
  when 2
    STDOUT.puts "FONT_DESCENT #{descent}"
  end
  STDOUT.puts "ENDPROPERTIES"
end

#--------------------------------------------------------------------------
#フォントデーターの読込み/加工
# 二重定義は考慮していません
#--------------------------------------------------------------------------
def read_data(file, font, shift_a, shift)
  chars = 0
  max_id = 0
  while line = file.gets
    if /STARTCHAR/ =~ line
      # ENCODING行
      /ENCODING\s+(-*\d+)/ =~ file.gets
      id = $1.to_i
      if id < 0
        id = 0xffff
      end
      # STARTCHAR行を保存
      font[id] = line
      font[id] += "ENCODING " + $1 + "\n"
      while line = file.gets
        # font[id]にENCODINGの次の行からENDCHARの前の行を保存
        if /.*BBX\s+(-*\d+)\s+(-*\d+)\s+(-*\d+)\s+(-*\d+)/ =~ line
          y = $4.to_i
          y = y - shift
          y = y + shift_a[id] if shift_a[id] != nil
          line = "BBX #{$1} #{$2} #{$3} #{y}\n"
        end
        if /.*ENDCHAR/ =~ line
          break
        end
        font[id] += line
      end
      chars += 1
      max_id = max(max_id, id)
    end
  end
  return max_id, chars
end

#--------------------------------------------------------------------------
# フォントデーターの合成
#--------------------------------------------------------------------------
def mix_data(num)
  chars = 0
  font_0 = 0
  font_1 = 0
  font_2 = 0

  for i in 0...0xffff
    type = 0
    if $font_0[i] != nil
      type += 1
    end
    if $font_1[i] != nil
      type += 2
    end

    case type
    when 0
    when 1
      $font_mix[i] = $font_0[i]
      chars += 1
      font_0 += 1
    when 2
      $font_mix[i] = $font_1[i]
      chars += 1
      font_1 += 1
    when 3
      # 同じフォントがあった場合
      if $map[i] != 1
        $font_mix[i] = $font_0[i]
      else
        $font_mix[i] = $font_1[i]
      end
      chars += 1
      font_2 += 1
    end
  end
  return chars, font_0, font_1, font_2
end

#--------------------------------------------------------------------------
# フォントデーター出力
#--------------------------------------------------------------------------
def out_data(num, max_char)
  STDOUT.puts "CHARS #{num}"
  for i in 0...max_char
    if $font_mix[i] != nil
      STDOUT.print $font_mix[i]
      STDOUT.puts "ENDCHAR"
    end
  end
  STDOUT.puts "ENDFONT"
end

#--------------------------------------------------------------------------
# 利用方法
#--------------------------------------------------------------------------
def usage
  STDERR.puts "usage: bdf_mix.rb [-s option] [-b option] bdf_file_1 bdf_file_2"
  exit 1
end

# max
def max(a, b)
  if b > a
    return b
  else
    return a
  end
end

#--------------------------------------------------------------------------
# 合成用マップデータの作成
#--------------------------------------------------------------------------
def map_set(num, start, last)
  start = eval(start)
  last = eval(last)
  start = 0 if start == nil
  last = 0xffff if last == nil
  if last < start
    temp = last; last = start; start = temp;
  end
  for loop in start..last
    $map[loop] = num - 1
  end
end

#--------------------------------------------------------------------------
# シフトデータの作成
#--------------------------------------------------------------------------
def shift_set(font, num, start, last)
  start = eval(start)
  last = eval(last)
  start = 0 if start == nil
  last = 0xffff if last == nil
  if last < start
    temp = last; last = start; start = temp;
  end
  for loop in start..last
    if font == 1
      $shift_0[loop] = num
    else
      $shift_1[loop] = num
    end
  end
end

#--------------------------------------------------------------------------
# -s option
#--------------------------------------------------------------------------
def opt_s(arg)
  case arg
  when /^(\d+),*$/
    map_set($1.to_i, "0x0000", "0xFFFF")
  when /(\d+),(\S*)-(\S*)$/
    map_set($1.to_i, $2, $3)
  end
end

#--------------------------------------------------------------------------
# -b option
#--------------------------------------------------------------------------
def opt_b(arg)
  case arg
  when /(\d+),(-*\d+),*$/
    shift_set($1.to_i, $2.to_i, "0x0000", "0xFFFF")
  when /(\d+),(-*\d+),(\S*)-(\S*)$/
    shift_set($1.to_i, $2.to_i, $3, $4)
  end
end

#--------------------------------------------------------------------------
# main
#--------------------------------------------------------------------------

if ARGV.size <= 1
  usage
end

while /^-/ =~ ARGV[0]
  case ARGV[0]
  when /-s/
    ARGV.shift
    opt_s(ARGV[0])
  when /-b/
    ARGV.shift
    opt_b(ARGV[0])
  end
  ARGV.shift
end

# ファイルのオープン
bdf_file_0 = open(ARGV[0])
bdf_file_1 = open(ARGV[1])

# ヘッダ部の読取り/出力
bx1, by1, bx2, by2, ascent, descent, shift = read_head(bdf_file_0, bdf_file_1)
write_head(bdf_file_0, bx1, by1, bx2, by2, ascent, descent)

# フォントデータの読取り/加工
last_0, num_0 = read_data(bdf_file_0, $font_0, $shift_0, 0)
last_1, num_1 = read_data(bdf_file_1, $font_1, $shift_1, shift)

max_char = max(last_0, last_1)

# フォントデータの合成
num, f0, f1, f2 = mix_data(max_char)

# フォントデータの出力
out_data(num, max_char)

# ファイルのクローズ
bdf_file_0.close
bdf_file_1.close

STDERR.puts
STDERR.puts "bdf mix : #{ARGV[0]} & #{ARGV[1]}"
STDERR.puts "#{ARGV[0]}:#{num_0} chars : use:#{f0} chars"
STDERR.puts "#{ARGV[1]}:#{num_1} chars : use:#{f1} chars"
STDERR.puts "overlap :#{f2} chars"
STDERR.puts "total #{num} chars"
exit
