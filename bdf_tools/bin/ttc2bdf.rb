#! /usr/bin/ruby
#
# ttc2bdf.rb ver 2.0
# TTCファイルをTTFに分割し、内部のbitmapデータからBDFファイルを作成
#
# 使い方
# ttc2bdf.rb ttc_file [font_name1 ...]
# font_name1を指定するとBDFファイル名に使用されます
# ※TTFファイルの数だけ指定が必要。指定無し、足りない場合は内部名を利用します
#
# 以下のファイルが必要となります
# sbitget.exe / TTFDUMP.EXE / ttc2ttf.exe
# ttf2bdf.rb bdf_remap.rb

#--------------------------------------------------------------------------
#定数の定義
#--------------------------------------------------------------------------
BREAKTTC = "./bin/ttc2ttf.exe"
TTF2BDF = "./bin/ttf2bdf.rb"

#--------------------------------------------------------------------------
# 利用方法
#--------------------------------------------------------------------------
def usage
  STDERR.puts "usage: ttc2bdf.rb ttc_file [font_name1 ...]"
  exit 1
end

#--------------------------------------------------------------------------
# main
#--------------------------------------------------------------------------

# 引数なしの場合
if ARGV.size == 0
  usage
end

font_file = []
font_num = 0

# TTCをTTFに分解
out = `#{BREAKTTC} #{ARGV[0]}`

# TTFファイルの数とファイル名を取得
out.each do |line|
  if /TTF\d+\s+:Start\s+Compleate\s+output:\s+(.+ttf)/ =~ line
    font_file[font_num] = $1
    STDERR.puts font_file[font_num]
    font_num += 1
  end
end

# TTFファイルが作成されなかった場合
if font_num == 0
  STDERR.puts "No TTF file"
  exit 1
end
STDERR.puts "Make TTF file #{font_num}."

# TTFファイルからbdfファイルを作成
for i in 0 ... font_num
  if ARGV[i + 1] != nil
    out = `#{TTF2BDF} #{font_file[i]} #{ARGV[i + 1]}`
  else
    out = `#{TTF2BDF} #{font_file[i]}`
  end
  File.delete(font_file[i])
end

exit
