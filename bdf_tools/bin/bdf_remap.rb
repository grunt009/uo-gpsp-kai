#! /usr/bin/ruby
#
# bdf_remap.rb ver 2.0
# sbitget.exe で作成したBDFファイルと、TTFDUMP.EXE で作成したdumpファイルから
# 実際に使用できるBDFファイルを作成
#
# 使い方
# bdf_remap.rb bdf_file dump_file
# BDFファイルは標準出力に出力
# dumpファイルは `TTFDUMP.exe -tcmap -nx FONT.TTF > FONT.DUMP` として作成しておく

#--------------------------------------------------------------------------
#グローバル変数の定義
#--------------------------------------------------------------------------
$map = []
$hmtx = []
$font = []
$isfixed = 1
$chars = 0

#--------------------------------------------------------------------------
# フォントデータ読込み
#--------------------------------------------------------------------------
def make_font
  bdf_file = open(ARGV[0])
  flag = 0

  #ヘッダー部分をENDPROPERTIESまでそのまま出力、CHARSは出力せずに保存
  while line = bdf_file.gets
    if /.*CHARS\s+(\d+)/ =~ line
      old_chars = $1.to_i
      break
    end
    if /FONTBOUNDINGBOX\s+(-*\d+)\s+(-*\d+)\s+(-*\d+)\s+(-*\d+)/ =~ line
      x1 = $1.to_i; y1 = $2.to_i; x2 = $3.to_i; y2 = $4.to_i
      ascent = y1 + y2
      descent = y1 - ascent
      STDOUT.puts "FONT_ASCENT #{ascent}"
      STDOUT.puts "FONT_DESCENT #{descent}"
    end
    STDOUT.print line
  end

  #フォントデーターの読込み
  # 二重定義は考慮していません
  while line = bdf_file.gets
    if /.*STARTCHAR\s+glyphID:([0-9a-fA-F]{4})/ =~ line
      id = $1.hex
      $font[id] = ''
      # ENCODING行を読捨て
      bdf_file.gets
      while line = bdf_file.gets
        if /.*ENDCHAR/ =~ line
          break
        end
        # $font[id]にENCODINGの次の行からENDCHARの前の行を保存
        $font[id] += line
      end
    end
  end

  bdf_file.close
  return old_chars
end

#--------------------------------------------------------------------------
# cmapファイルからマップを作成
#--------------------------------------------------------------------------
def read_dump
  map_file = open(ARGV[1])

  # OS/2 データからプロポーショナルかどうか判別
  while line = map_file.gets
    if /\s*PANOSE:\s*\d+\s+\d+\s+\d+\s+(\d+)/ =~ line
      $isfixed = $1.to_i
      break
    end
  end

  # マップファイル作成
  while line = map_file.gets
    if /.*Char\s+([0-9a-fA-F]{4})\s+->\s+Index\s+(\d+)/ =~ line
      #2番目以降のcmapは読取らない
      break if $map[$1.hex] != nil
      $map[$1.hex] = $2.to_i

      # 定義されているフォントだけを数える
      $chars += 1 if $font[$2.to_i] != nil
    end
    break if /Table - Horizontal Metrics/ =~ line
  end

  if $isfixed == 0
    # hmtx データを作成
    while line = map_file.gets
      if /\s*(\d+)\..+advWid:\s+(\d+),/ =~ line
        $hmtx[$1.to_i] = $2.to_i
      end
    end
  end

  map_file.close
end

#--------------------------------------------------------------------------
# 変換後のデーター出力
#--------------------------------------------------------------------------
def out_font
  STDOUT.print "CHARS ",$chars,"\n"

  loop = 0
  char = 0
  width = 0
  temp = 0
  while loop < $chars
    if $map[char] != nil
      if $font[$map[char]] != nil
        STDOUT.print "STARTCHAR U+",format("%04X", char),"\n"
        STDOUT.print "ENCODING ",char,"\n"

        $font[$map[char]].each_line {|line|
          if $isfixed == 0
            if /.*DWIDTH\s+(\d+)/ =~ line
              width = (($1.to_i * $hmtx[$map[char]]) / 256).ceil
              line = ""
            end
            if /.*BBX\s+(-*\d+)\s+-*\d+\s+(-*\d+)\s+-*\d+/ =~ line
              temp = $1.to_i + $2.to_i
              width = temp if width < temp
              line = "DWIDTH #{width}\n#{line}"
            end
          end
        STDOUT.print line
        }

        STDOUT.print "ENDCHAR\n"
        loop += 1
      end
    end
    char += 1
  end
  STDOUT.print "ENDFONT\n"
end

#--------------------------------------------------------------------------
# 利用方法
#--------------------------------------------------------------------------
def usage
  STDERR.print "usage: bdf_remap.rb bdf_file cmap_dump\n"
  exit 1
end

#--------------------------------------------------------------------------
#main
#--------------------------------------------------------------------------
if ARGV.size <= 1
  usage
end

STDERR.print "convert font\n"
num = make_font

STDERR.print "read dump\n"
read_dump

out_font
STDERR.print "convert end\n"
STDERR.print "Read char:",num,"  Write char:",$chars,"\n"
exit
