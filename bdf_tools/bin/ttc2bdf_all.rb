#! /usr/bin/ruby
#
# ttc2bdf_all.rb ver 2.0
# 指定したディレクトリのすべてのTTCファイルをTTFに分割し、内部のbitmapデータからBDFファイルを作成
#
# 以下のファイルが必要となります
# sbitget.exe / TTFDUMP.EXE / ttc2ttf.exe
# ttc2bdf.rb / ttf2bdf.rb / bdf_remap.rb

#--------------------------------------------------------------------------
#定数の定義
#--------------------------------------------------------------------------
TTC2BDF = "./bin/ttc2bdf.rb"

Dir::glob(ARGV[0]+"/*.ttc").each {|f|
 puts "ttc2bdf #{f}"
 out = `#{TTC2BDF} #{f}  2>/dev/null`
}

exit
