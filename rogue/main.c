#include <pspkernel.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#include "rogue.h"

/**************************************************************************
	変数定義
**************************************************************************/

extern short party_room;
#ifndef ORIGINAL
extern char *nick_name;
char mesg[507][80];	/* for separation */
#endif

extern short buffer[DROWS][DCOLS];
extern boolean score_only;

extern char *rest_file;
extern boolean do_restore;

extern char login_name[30];

/* windows */

char strmesg[256];
char strdefname[30];

char strfile[256];
short ch = 0;
int reset = 0;

/**************************************************************************
	終了処理
**************************************************************************/


/**************************************************************************
	main
**************************************************************************/

//int xmain(int argc, char *argv)
int main(int argc, char **argv)
{
	int ret = 0;
	char str[MAX_PATH];

#ifndef PSPE
	strcpy(str, *argv);
	*(strrchr(str, '/')+1) = 0;
	chdir(str);
	strcpy(org_dir, str);
#endif

#ifndef ORIGINAL
	int first = 1;
	char buf[80];
#endif

//	SetupCallbacks();
	pgInit();
	pgScreenFrame(1,0);

	loadINI();

	/* Rogue 初期化 */
	memset(buffer,0x20,DROWS*DCOLS*sizeof(short));

	/* mesg file */
	strcpy(strmesg,"mesg.msg");
	if((ret = read_mesg(strmesg)) != 0)
	{
		/*
		if(ret < 0)
		{
			sprintf(str,"%s が開けません！",strmesg);
			MessageBox(hWnd,str,0,0);
		}
		else if(ret > 0)
		{
			sprintf(str,"%s の %d 番目にエラーがあります！",strmesg,ret);
			MessageBox(hWnd,str,0,0);
		}

		MessageBox(hWnd,"代わりに mesg.msg を開きます",strAppName,0);
		*/
		if((ret = read_mesg("mesg.msg")) != 0)
		{
//			MessageBox(hWnd,"またエラーです。終了します。",0,0);
			exit(0);
		}
	}

	/* s: score r: restore */
/*	if (init(argc - 1, argv + 1))*/
INIT:
	reset = 0;
	if(init(0,""))
	{	/* restored game */
		goto PL;
	}

LOAD:

	if(reset) /* load file */
	{
		restore(rest_file);
		reset = 0;
		goto PL;
	}

	for (;;)
	{
		clear_level();
		make_level();
		put_objects();
		put_stairs();
		add_traps();
		put_mons();
		put_player(party_room);
		print_stats(STAT_ALL);
#ifndef ORIGINAL
		if (first)
		{
			sprintf(buf,mesg[10],nick_name);
			message(buf, 0);
		}
	PL:
		first = 0;
#else
	PL:
#endif
		play_level(); /* 無限ループ */
		free_stuff(&level_objects);
		free_stuff(&level_monsters);

		if(reset) goto INIT; /* windows */
	}

	exit(0);
}

short getcharPSP()
{
	if(bSleep){
		while(bSleep) pgWaitV();
	}
#ifndef PSPE
	pgWaitV();
#endif
	ch = 0;
	readpad();
	if(new_pad & CTRL_TRIANGLE){
		ch = keyboard();
		refreshw();
	}else if(new_pad&CTRL_UP && new_pad&CTRL_LEFT){
		ch = 'y';
	}else if(new_pad&CTRL_UP && new_pad&CTRL_RIGHT){
		ch = 'u';
	}else if(new_pad&CTRL_DOWN && new_pad&CTRL_LEFT){
		ch = 'b';
	}else if(new_pad&CTRL_DOWN && new_pad&CTRL_RIGHT){
		ch = 'n';
	}else if(new_pad & CTRL_UP && !(now_pad&CTRL_RTRIGGER)){
		ch = 'k';
	}else if(new_pad & CTRL_DOWN && !(now_pad&CTRL_RTRIGGER)){
		ch = 'j';
	}else if(new_pad & CTRL_LEFT && !(now_pad&CTRL_RTRIGGER)){
		ch = 'h';
	}else if(new_pad & CTRL_RIGHT && !(now_pad&CTRL_RTRIGGER)){
		ch = 'l';
	}else if(new_pad & CTRL_LTRIGGER){
		ch = 'i';
	}else if(new_pad & CTRL_CIRCLE){
		ch = '.';
	}else if(new_pad & CTRL_SQUARE){
		ch = 's';
	}else if(new_pad & CTRL_START){
		ch = 'S';
	}else if(new_pad & CTRL_SELECT){
		ch = 't';
	}
	if(new_pad & CTRL_CROSS){
		if(ch)	ch = CTRL(ch);
		else	ch = '\033';
	}

	return ch;
}
void  charreset (){ ch = 0;}

/*************************************************************************/

/* メッセージファイルを読み込む
-：ファイルがないとかで開けない。
0：正常
+：その番号にエラーが。
 */
int read_mesg(char *strmesg)
{
	FILE *mesg_file;
	char buf[256];
	int i, n, s, e;

	if ((mesg_file = fopen(strmesg, "r")) == NULL) return -1;

	while (fgets(buf, 256, mesg_file) != NULL) /* 一行の最大文字数は 256byte まで */
	{
		if ((n = atoi(buf)) > 0 && n < 500)
		{
			/* " を探す */
			for (i = 0; i < 256 && buf[i] && buf[i] != '\"'; ++i);

			if(i< 256 && buf[i]) s = i + 1;
			else return n;

			/* " を探す */
			for (i = s; i < 256 && buf[i] && buf[i] != '\"'; ++i);

			if(i< 256 && buf[i]) e = i - 1;
			else return n;

			/* メッセージ読み出し */
			for (i = 0; i < e-s+1 && i < 79; ++i) mesg[n][i] = buf[s + i];
			mesg[n][i] = '\0';
		}
	}
	fclose(mesg_file);

	return 0;
}

int loadINI()
{
	FILE *fp = fopen("NAME.txt","r");
	if(fp==NULL){
#ifdef JAPAN
		strcpy(strdefname, "戦士");
#else
		strcpy(strdefname, "Fighter");
#endif
		if ((fp=fopen("NAME.txt", "w")) != NULL){
			fwrite(strdefname, 1, strlen(strdefname), fp);
			fclose(fp);
		}
	}else{
		memset(strdefname, 0, sizeof(strdefname));
		fread(strdefname, 1, sizeof(strdefname)-1, fp);
		fclose(fp);
		int i;
		for(i=0; i<sizeof(strdefname)-1; i++){
			if(strdefname[i] == 015){
				strdefname[i] = 0;
				break;
			}
		}
	}

	return 0;
}

int saveINI()
{
	return 0;
}

char keyboard(void)
{
	enum{
		KEY_X = 336,
		KEY_Y = 16,
		FONT_H = 10,
		FONT_W = 5,
	};
	int x,y,i,row;
	static int sel=0;
	char str[2];
	unsigned long color, bgcolor;
	const char *KEY="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789   ()[]!?#$%&@;:<>=+-*/'^.,_ ";
	const int	KEY_LEN = strlen(KEY),
				COLS = 13,
				ROWS = KEY_LEN / COLS;

	pgFillBox(KEY_X-5, KEY_Y-5, KEY_X+FONT_W*2*COLS, KEY_Y+FONT_H*ROWS+5, 0);
	pgDrawFrame(KEY_X-5, KEY_Y-5, KEY_X+FONT_W*2*COLS, KEY_Y+FONT_H*ROWS+5, 0xffff);
	for(;;){
		row = sel/COLS;
		readpad();
		if(new_pad & CTRL_CIRCLE){
			return KEY[sel];
		}else if(new_pad & CTRL_CROSS){
			return '\033';
		}else if(new_pad & CTRL_UP){
			sel-=COLS;
			if(sel<0) sel+=KEY_LEN;
		}else if(new_pad & CTRL_DOWN){
			sel+=COLS;
			if(sel>=KEY_LEN) sel-=KEY_LEN;
		}else if(new_pad & CTRL_LEFT){
			sel-=1;
			if(sel/COLS != row) sel+=COLS;
			else if(sel == -1)	sel=COLS-1;
		}else if(new_pad & CTRL_RIGHT){
			sel+=1;
			if(sel/COLS != row) sel-=COLS;
		}else if(new_pad & CTRL_LTRIGGER){
			return '*';
		}else if(new_pad & CTRL_TRIANGLE){
			return 0;
		}else if(new_pad & CTRL_SQUARE){
			return '\b';
		}else if(new_pad & CTRL_START){
			return '\n';
		}
		pgWaitV();
		for(i=0; i<KEY_LEN; i++){
			str[0]=KEY[i]; str[1]=0;
			if(sel==i){
				color   = RGB(0xff,0xff,0xff);
				bgcolor = RGB(0xff,0x00,0x00);
			}else{
				color   = RGB(0xff,0xff,0xff);
				bgcolor = RGB(0x00,0x00,0x00);
			}
			mh_print(	KEY_X + (i%COLS)*FONT_W*2,
						KEY_Y + (i/COLS)*FONT_H, str, color, bgcolor);
		}
	}
}

