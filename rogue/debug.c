#include "debug.h"

void DebugPrint(const char* str, ...) {
#if defined PSPE || defined DEBUG
	va_list ap;
	char szBuf[256];

	va_start(ap, str);
	vsprintf(szBuf, str, ap);
	va_end(ap);
#ifdef PSPE
	pspeDebugWrite(szBuf,strlen(szBuf));
#endif
#ifdef DEBUG
	FILE *fp = fopen("debug.txt","a");
	if(fp!=NULL){
		fwrite(szBuf,1,strlen(szBuf),fp);
		fclose(fp);
	}
#endif
#endif
}

