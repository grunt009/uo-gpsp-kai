#ifndef __CISO_H__
#define __CISO_H__

#include <pspiofilemgr.h>

typedef struct ciso_header
{
    unsigned char magic[4];         /* +00 : 'C','I','S','O'                 */
    unsigned long header_size;      /* +04 : header size (==0x18)            */
    unsigned long long total_bytes; /* +08 : number of original data size    */
    unsigned long block_size;       /* +10 : number of compressed block size */
    unsigned char ver;              /* +14 : version 01                      */
    unsigned char align;            /* +15 : align of index value            */
    unsigned char rsv_06[2];        /* +16 : reserved                        */
}CISO_H;

#define CISO_HEADER_SIZE (0x18)

int cso_read(char *o_buff, const char *path, int sector, int o_size);
int cso_read_fp(char *buf, SceUID fp, int pos, int size);
int cso_write(const char *buf, const char *path, int pos, int size, int level);

int inflate_cso(char* o_buff, int o_size, const char* i_buff, int i_size);
int deflate_cso(char* o_buff, int o_size, const char* i_buff, int i_size, int level);
int auto_deflate_cso(char* o_buff, int o_size, const char* i_buff, int i_size, int total_size);

#endif
