/*
 * eboot.c
 *
 *  Created on: 2009/12/29
 *      Author: takka
 */

#include "file.h"
#include "error.h"

int eboot_read(void *buf, int max_buf, const char* path, file_type type)
{
  int pos;
  int size;
  int size_pos;
  int ret;

  ret = get_file_data(&pos, &size, &size_pos, path, type, "EBOOT.BIN");
  if(ret < 0)
    return ret;
  else if(size > max_buf)
    return ERR_SIZE_OVER;

  ret = file_read(buf, path, type, pos, size);

  return ret;
}

int eboot_write( void *buf, int size, const char* path, file_type type)
{
  int pos;
  int orig_size;
  int size_pos;
  int ret;

  ret = get_file_data(&pos, &orig_size, &size_pos, path, type, "EBOOT.BIN");
  if(ret < 0)
    return ret;

  ret = file_write(&size, path, type, size_pos, 4);
  if(ret < 0)
    return ret;

  ret = file_write(buf, path, type, pos, size);

  return ret;
}


