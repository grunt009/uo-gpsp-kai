/*
 * menu.c
 *
 *  Created on: 2010/01/06
 *      Author: takka
 */

#include <pspdebug.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <psprtc.h>
#include <psputility.h>
#include <psputility_sysparam.h>
#include <pspumd.h>
#include <systemctrl.h>
#include <systemctrl_se.h>
#include <pspctrl_kernel.h>

#include "menu.h"

#include "main.h"
#include "screen.h"
#include "pspdecrypt.h"
#include "sound.h"
#include "ciso.h"
#include "umd.h"
#include "eboot.h"
#include "unicode.h"
#include "fnt_print.h"
#include "key.h"
#include "web.h"

#define ERR_RET(err_data, ret_code) \
if((err_data) < 0)          \
{                           \
    err_msg((err_data));    \
    return ret_code;        \
}                           \

#define ERR_RET_2(err_data, err_num) \
if((err_data) < 0)          \
{                           \
    err_msg((err_num));     \
    ret = -1;               \
    goto LABEL_ERR;         \
}                           \

int nop(char *dir, char *file, file_type type, int opt_1, int opt_2);

/*---------------------------------------------------------------------------
  EBOOT_BINの変換
---------------------------------------------------------------------------*/
int eboot_exchange(char *dir, char *file, file_type type, int opt_1, int opt_2);
int eboot_recovery(char *dir, char *file, file_type type, int opt_1, int opt_2);
int rename_file(char *dir, char *file, file_type type, int opt_1, int opt_2);
int iso2cso(char *dir, char *file, file_type type, int opt_1, int opt_2);
int umd2iso(char *dir, char *file, file_type type, int opt_1, int opt_2);
int umd2cso(char *dir, char *file, file_type type, int opt_1, int opt_2);
int cso2iso(char *dir, char *file, file_type type, int opt_1, int opt_2);
int iso_patch(char *dir, char *file, file_type type, int opt_1, int opt_2);
int file_update(char *dir, char *file, file_type type, int opt_1, int opt_2);
int soft_reboot(char *dir, char *file, file_type type, int opt_1, int opt_2);
int remove_file(char *dir, char *file, file_type type, int opt_1, int opt_2);
int fix_header(char *dir, char *file, file_type type, int opt_1, int opt_2);
int boot_iso(char *dir, char *file, file_type type, int opt_1, int opt_2);

int file_trans(char* out_path, char *in_path, trans_type type, int level, int limit, int opt_1, int opt_2);

menu_item menu_sys[] = {
    { MENU_COMMAND, "UMD_ID.csv更新", (file_update), NULL, (int)"UMD_ID.csv", 0 },
    { MENU_COMMAND, "iso_tool更新",   (file_update), NULL, (int)"EBOOT.PBP", 1 },
    { MENU_NOP,     "--------------", (NULL), NULL, 0, 0 },
    { MENU_COMMAND, "再起動",         (soft_reboot), NULL, 0, 0 },
    { MENU_COMMAND, "終了",           (soft_reboot), NULL, 1, 0 },
    { MENU_NOP, "\0",                 (NULL), NULL, 0, 0 }
};

menu_item menu_iso[] = {
    { MENU_COMMAND, "EBOOT変換", (eboot_exchange), NULL, 0, 0 },
    { MENU_COMMAND, "EBOOTリカバリ", (eboot_recovery), NULL, 0, 0 },
    { MENU_COMMAND, "リネーム", (rename_file), NULL, 0, 0 },
    { MENU_COMMAND, "CSO変換", (iso2cso), NULL, 2048, 0 },
//    { MENU_COMMAND, "起動", (boot_iso), NULL, 2048, 0 },
    { MENU_NOP,     "--------------", (NULL), NULL, 0, 0 },
    { MENU_COMMAND, "削除", (remove_file), NULL, 0, 0 },
    { MENU_NOP, "\0",            (NULL), NULL, 0, 0 }
};

menu_item menu_cso[] = {
    { MENU_COMMAND, "EBOOT変換", (eboot_exchange), NULL, 0, 0 },
    { MENU_COMMAND, "EBOOTリカバリ", (eboot_recovery), NULL, 0, 0 },
    { MENU_COMMAND, "リネーム", (rename_file), NULL, 0, 0 },
    { MENU_COMMAND, "ISO変換", (cso2iso), NULL, 0, 0 },
    { MENU_NOP,     "--------------", (NULL), NULL, 0, 0 },
//    { MENU_COMMAND, "ヘッダー調整", (fix_header), NULL, 0, 0 }, // TODO
    { MENU_COMMAND, "削除", (remove_file), NULL, 0, 0 },
    { MENU_NOP, "\0"    , (NULL), NULL, 0, 0 }
};

menu_item menu_umd[] = {
    { MENU_COMMAND, "ISO変換", (umd2iso), NULL, 0, 0 },
    { MENU_COMMAND, "CSO変換", (umd2cso), NULL, 0, 0 },
    { MENU_NOP, "\0"    , (NULL), NULL, 0, 0 }
};

menu_item menu_dir[] = {
    { MENU_NOP, "未実装", (NULL), NULL, 0, 0 },
    { MENU_NOP, "\0"    , (NULL), NULL, 0, 0 }
};

menu_item select_yes_no[] = {
    { MENU_RET_INT, "はい", (nop), NULL, YES, 0 },
    { MENU_RET_INT, "いいえ", (nop), NULL, NO, 0 },
    { MENU_NOP, "\0"    , (NULL), NULL, 0, 0 }
};

menu_item select_no_yes[] = {
    { MENU_RET_INT, "いいえ", (nop), NULL, NO, 0 },
    { MENU_RET_INT, "はい", (nop), NULL, YES, 0 },
    { MENU_NOP, "\0"    , (NULL), NULL, 0, 0 }
};

menu_item select_rename[] = {
    { MENU_RET_INT, "通常", (nop), NULL, 0, 1 },
    { MENU_RET_INT, "UMD ID", (nop), NULL, 1, 0 },
    { MENU_RET_INT, "日本語名", (nop), NULL, 2, 1 },
    { MENU_RET_INT, "英語名", (nop), NULL, 3, 0 },
    { MENU_NOP, "\0"    , (NULL), NULL, 0, 0 }
};

menu_item select_cso_level[] = {
    { MENU_RET_INT, "9", (nop), NULL, 9, 0 },
    { MENU_RET_INT, "8", (nop), NULL, 8, 0 },
    { MENU_RET_INT, "7", (nop), NULL, 7, 0 },
    { MENU_RET_INT, "6", (nop), NULL, 6, 0 },
    { MENU_RET_INT, "5", (nop), NULL, 5, 0 },
    { MENU_RET_INT, "4", (nop), NULL, 4, 0 },
    { MENU_RET_INT, "3", (nop), NULL, 3, 0 },
    { MENU_RET_INT, "2", (nop), NULL, 2, 0 },
    { MENU_RET_INT, "1", (nop), NULL, 1, 0 },
    { MENU_NOP, "\0"    , (NULL), NULL, 0, 0 }
};

menu_item select_cso_threshold[] = {
    { MENU_RET_INT, "100%", (nop), NULL, 100, 0 },
    { MENU_RET_INT, " 95%", (nop), NULL, 95, 0 },
    { MENU_RET_INT, " 90%", (nop), NULL, 90, 0 },
    { MENU_RET_INT, " 85%", (nop), NULL, 85, 0 },
    { MENU_RET_INT, " 80%", (nop), NULL, 80, 0 },
    { MENU_RET_INT, " 75%", (nop), NULL, 75, 0 },
    { MENU_RET_INT, " 70%", (nop), NULL, 70, 0 },
    { MENU_RET_INT, " 65%", (nop), NULL, 65, 0 },
    { MENU_RET_INT, " 60%", (nop), NULL, 60, 0 },
    { MENU_RET_INT, " 55%", (nop), NULL, 55, 0 },
    { MENU_RET_INT, " 50%", (nop), NULL, 50, 0 },
    { MENU_NOP, "\0"    , (NULL), NULL, 0, 0 }
};

menu_list eboot_exchange_menu[] = {
    { "EBOOT.BINのバックアップを保存しますか？", "バックアップ", select_no_yes, &global.eboot_backup, 0 },
    { "自動パッチ機能を有効にしますか？", "自動パッチ", select_no_yes, &global.auto_patch, 0 },
    { "開始しますか？", "開始", select_no_yes, NULL, 0 },
    { "", "", NULL, NULL, 0 }
};

menu_list cso_trans_menu[] = {
    { "圧縮レベル 1(低圧縮)～9(高圧縮)", "圧縮レベル", select_cso_level, &global.cso_level, 0 },
    { "閾値 50%(低圧縮)～100%(高圧縮)", "閾値", select_cso_threshold, &global.cso_threshold, 0 },
    { "", "", NULL, NULL, 0 }
};

char *file_ext[] = { ".ISO", ".CSO" };

/*---------------------------------------------------------------------------
  NOP
---------------------------------------------------------------------------*/
int nop(char *dir, char *file, file_type type, int opt_1, int opt_2)
{
  return 0;
}

/*---------------------------------------------------------------------------
  EBOOT_BINの変換
---------------------------------------------------------------------------*/
int eboot_exchange(char *dir, char *file, file_type type, int opt_1, int opt_2)
{
  int i_size;
  int o_size;
  char path[MAX_PATH];
  char ebt_path[MAX_PATH];
  const unsigned char patch_1[] = { 0xE0, 0xFF, 0xBD, 0x27, 0x05, 0x05, 0x02, 0x3C, 0x0C, 0x00, 0xB3, 0xAF};
  const unsigned char patch_2[] = { 0x00, 0x40, 0x05, 0x34, 0x05, 0x05, 0x04, 0x3C, 0x99, 0x81, 0x05, 0x0C};

  int num;
  int ret;
  int mod_flag = 0;
  char *write_ptr = &WORK[DECRYPT_DATA][0];
  char *read_ptr = &WORK[CRYPT_DATA][0];

  strcpy(path, dir);
  strcat(path, file);

  msg_win("", 0, MSG_CLEAR, 0);
  ret = select_menu_list(eboot_exchange_menu);
  if(ret != YES)
    return CANCEL;

  msg_win("EBOOT.BINを読込んでいます", 1, MSG_WAIT, 1);

  // EBOOT.BIN読込み
  i_size = eboot_read(read_ptr, EBOOT_MAX_SIZE, path, type);
  ERR_RET(i_size, DONE);

  // バックアップ
  // 複合済みか判定
  if(strncmp(read_ptr, "~PSP" , 4) == 0)
  {
    if(global.eboot_backup == YES)
    {
      // EBTファイルの書込み
      msg_win("EBOOT.BINを保存しています", 1, MSG_WAIT, 1);

      strcpy(ebt_path, "BACK_UP/");
      strcat(ebt_path, global.umd_id);
      strcat(ebt_path, ".EBT");

      ret = ms_write(read_ptr, ebt_path, 0, i_size);
      ERR_RET(ret, DONE);
    }
  }

  if(strncmp(&read_ptr[1], "ELF" , 3) == 0)
  {
    // 複合化ずみ
    msg_win("EBOOT.BINは復号ずみです", 1, MSG_WAIT, 1);
    o_size = i_size;
    write_ptr = read_ptr;
  }
  else
  {
    // 複合化
    msg_win("EBOOT.BINを復号しています", 1, MSG_WAIT, 1);
    o_size = pspDecryptPRX((u8 *)read_ptr, (u8 *)write_ptr, i_size);
    ERR_RET(o_size, DONE);

    if(strncmp(&write_ptr[1], "ELF", 3) != 0)
    {
      err_msg(ERR_DECRYPT);
      return DONE;
    }

    mod_flag = 1;
  }

  // パッチ
  //    addiu  r29,r29,-$20                       ;0000010C[27BDFFE0,'...''] E0 FF BD 27
  // +  lui    r2,$0505                           ;00000110[3C020505,'...<'] 05 05 02 3C
  //    sw     r19,$c(r29)                        ;00000114[AFB3000C,'....'] 0C 00 B3 AF

  //47 81 05 0C 00 40 05 34 05 05 04 3C 99 81 05 0C

  if(global.auto_patch == 1)
  {
    num = 0;
    do{
      if(memcmp(&write_ptr[num], patch_1, 12) == 0)
      {
        write_ptr[num + 4] = 0;
        msg_win("EBOOT.BINにパッチをあてました", 1, MSG_WAIT, 1);
        mod_flag = 1;
      }
      if(memcmp(&write_ptr[num], patch_2, 12) == 0)
      {
        write_ptr[num + 4] = 0;
        msg_win("EBOOT.BINにパッチをあてました", 1, MSG_WAIT, 1);
        mod_flag = 1;
      }
      num++;
    }while(num < o_size);
  }

  // ISOファイルへの書込み
  // ファイルサイズの変更
  if(mod_flag == 1)
  {
    msg_win("EBOOT.BINを書き込んでいます", 1, MSG_WAIT, 1);

    ret = set_file_mode(path, FIO_S_IWUSR | FIO_S_IWGRP | FIO_S_IWOTH);
    ERR_RET(ret, DONE);

    ret = eboot_write(write_ptr, o_size, path, type);
    ERR_RET(ret, DONE);
  }

  msg_win("終了しました", 1, MSG_WAIT, 6);
  msg_win("", 0, MSG_CLEAR, 0);
  return 0;
}

/*---------------------------------------------------------------------------
  EBOOT_BINのリカバリ
---------------------------------------------------------------------------*/
int eboot_recovery(char *dir, char *file, file_type type, int opt_1, int opt_2)
{
  char path[MAX_PATH];
  char ebt_path[4][MAX_PATH];
  int ebt_size;
  int loop;
  int ret;

  strcpy(path, dir);
  strcat(path, file);

  msg_win("", 0, MSG_CLEAR, 0);
  msg_win("EBOOT.BINを読込んでいます", 1, MSG_WAIT, 1);

  // EBOOT.BIN読込み
  strcpy(ebt_path[0], "BACK_UP/");
  strcat(ebt_path[0], global.umd_id);
  strcat(ebt_path[0], ".EBT");

  strcpy(ebt_path[1], "BACK_UP/");
  strcat(ebt_path[1], file);
  strcpy(&ebt_path[1][strlen(ebt_path[1]) - 3], "EBT");

  strcpy(ebt_path[2], dir);
  strcat(ebt_path[2], global.umd_id);
  strcat(ebt_path[2], ".EBT");

  strcpy(ebt_path[3], path);
  strcpy(&ebt_path[3][strlen(ebt_path[3]) - 3], "EBT");

  for(loop = 0; loop < 4; loop++)
  {
    ebt_size = ms_read(&WORK[CRYPT_DATA][0], ebt_path[loop], 0, 0);
    if(ebt_size > 0)
      break;
  }
  ERR_RET(ebt_size, DONE);

  // ISOファイルへの書込み
  // ファイルサイズの変更
  msg_win("EBOOT.BINをリカバリしています", 1, MSG_WAIT, 1);
  ret = set_file_mode(path, FIO_S_IWUSR | FIO_S_IWGRP | FIO_S_IWOTH);
  ERR_RET(ret, DONE);

  ret = eboot_write(&WORK[CRYPT_DATA][0], ebt_size, path, type);
  ERR_RET(ret, DONE);

  msg_win("終了しました", 1, MSG_WAIT, 6);
  msg_win("", 0, MSG_CLEAR, 0);
  return DONE;
}

int rename_file(char *dir, char *file, file_type type, int opt_1, int opt_2)
{
  char path[MAX_PATH];
  char new_path[MAX_PATH];
  char id[MAX_PATH];
  char j_name[MAX_PATH];
  char e_name[MAX_PATH];
  char *name = NULL;
  char work[MAX_PATH];
  char work2[MAX_PATH];
  char *text[15];
  int name_type;
  int ret;
  int mode = 0;
  char *ptr;
  int loop;
  char bad_char[] = "<>:\"/\\|?*";
  char good_char[] = "()_\'_____";

  msg_win("", 0, MSG_CLEAR, 0);
  msg_win("ファイル名変更", 1, MSG_WAIT, 1);
  name_type = select_menu("種類", select_rename, 0, 25, 8);

  if(name_type < 0)
    return -1;
  else
  {
    strcpy(path, dir);
    strcat(path, file);
    get_umd_id(id, path, type);
    get_umd_name(j_name, e_name, id, 0);

    switch(name_type)
    {
      case 0: // リネーム
        strcpy(j_name, file);
        j_name[strlen(j_name) - 4] = '\0';
        sjis_to_utf8(e_name, j_name);
        name = e_name;
        mode = 1;
        break;

      case 1: // UMD ID
        name = id;
        mode = 0;
        break;

      case 2: // 日本語名
        name = j_name;
        mode = 1;
        break;

      case 3: // 英語名
        name = e_name;
        mode = 0;
        break;

    }

    ret = osk(work, name, "リネーム", mode);

    if(ret == PSP_UTILITY_OSK_RESULT_CHANGED)
    {
      // 駄目文字チェック
      // < > : " / \ | ? *
      sjis_to_utf8(work2, work);

      for(loop = strlen(bad_char); loop > 0; loop--)
      {
        ptr =strchr(work2, bad_char[loop - 1]);
        if(ptr != NULL)
        {
          *ptr = good_char[loop - 1];
          loop++;
        }
      }

      // 先頭の「.」対策
      if(work2[0] == '.')
      {
        strcpy(work, work2);
        work2[0] = ' ';
        strcpy(&work2[1], work);
      }

      utf8_to_utf16((u16*)work, work2);
      utf16_to_sjis(work2, work);

      strcpy(new_path, dir);
      strcat(new_path, work2);
      strcat(new_path, file_ext[type]);

      ret = sceIoRename(path, new_path);
      if(ret < 0)
      {
          err_msg(ERR_RENAME);
          return DONE;
      }

      text[0] = "リネーム完了";
      text[1] = "◎を押してください";
      text[2] = "\0";
      dialog(text);
    }
  }

  return DONE;
}

// TODO
int iso_patch(char *dir, char *file, file_type type, int opt_1, int opt_2)
{
  return CANCEL;
}

// TODO
int umd2iso(char *dir, char *file, file_type type, int opt_1, int opt_2)
{
  char in_path[MAX_PATH];
  char out_path[MAX_PATH];
  char ren_path[MAX_PATH];
  char work[MAX_PATH];
  char *read_buf[3];                // 読込バッファ ポインタ
  char *write_buf[3];               // 書込バッファ ポインタ

  SceUID fp_in = 0;                 // 読込ファイル ポインタ
  SceUID fp_out = 0;                // 書込ファイル ポインタ
  SceInt64 res = -1;                // 非同期ファイルIO用

  const int MAX_READ_SIZE = 512;    // 一回の読込サイズ

  int in_sec_num = 0;               // 総セクタ数
  int read_size[3];                 // 読込んだサイズ
  int write_size = 0;               // 一回の書込サイズ
  int write_sector = 0;             // 処理したセクタ数
  int read_sector = 0;              // 処理したセクタ数

  int num;                          // 汎用
  int ret;

  char *text[15];                   // テキスト表示用
  char msg[256];                    // テキスト表示用

  SceCtrlData  data;
  u64 start_tick;
  u64 now_tick;
  u64 old_tick = 0;
  pspTime date1;
  pspTime date2;
  pspTime date3;
  int first_wait = 0;

  // buff 設定
  read_buf[0] = &WORK[O_BUFFER][0];
  read_buf[1] = &WORK[O_BUFFER + MAX_READ_SIZE][0];
  read_buf[2] = &WORK[O_BUFFER + MAX_READ_SIZE * 2][0];
  write_buf[0] = &WORK[O_BUFFER][0];
  write_buf[1] = &WORK[O_BUFFER + MAX_READ_SIZE][0];
  write_buf[2] = &WORK[O_BUFFER + MAX_READ_SIZE * 2][0];

  // MSチェック
  ret = check_ms();
  if(ret < 0)
    return CANCEL;

  // UMDチェック
  ret = check_umd();
  if(ret < 0)
    return CANCEL;

  // 空き容量チェック
  if((global.umd_size / 1024) > global.ms_free_size)
  {
    err_msg(ERR_SIZE_OVER);
    return CANCEL;
  }

  // ファイル名設定
  strcpy(in_path, dir);
  strcat(in_path, file);

  strcpy(out_path, dir);
  ret = osk(work, global.umd_id, "ファイル名指定", 0);
  if(ret == PSP_UTILITY_OSK_RESULT_CHANGED)
    strcat(out_path, work);
  else
    strcat(out_path, global.umd_id);
  strcat(out_path, ".ISO");

  strcpy(ren_path, dir);
  strcat(ren_path, "01234565789012345678901234567890123456789012345678901234567890123456789");

  msg_win("", 0, MSG_CLEAR, 0);
  sjis_to_utf8(work, strrchr(out_path, '/') + 1);
  sprintf(msg, "出力名：%s", work);
  msg_win(msg, 0, MSG_WAIT, 0);

  if(check_file(out_path) < 0)
  {
    msg_win("開始しますか？", 0, MSG_WAIT, 0);
    ret = select_menu("開始", select_yes_no, 0, 28, 10);
  }
  else
  {
    msg_win("同名のファイルがあります", 0, MSG_WAIT, 0);
    msg_win("上書きしますか？", 0, MSG_WAIT, 0);
    ret = select_menu("上書き", select_no_yes, 0, 28, 10);
  }

  if(ret != YES)
    return CANCEL;

  in_sec_num = get_umd_sector("umd:", TYPE_UMD);

  // open設定
  fp_in  = sceIoOpen("umd:", PSP_O_RDONLY, 0777);
  ERR_RET_2(fp_in, ERR_OPEN);
  sceIoLseek32(fp_in, 0, SEEK_SET);

  fp_out = sceIoOpen(ren_path, PSP_O_WRONLY | PSP_O_CREAT | PSP_O_TRUNC, 0777);
  ERR_RET_2(fp_out, ERR_OPEN);
  sceIoLseek32(fp_out, 0, SEEK_SET);

  // msg
  msg_win("", 0, MSG_CLEAR, 0);
  msg_win("UMD -> ISO 変換", 0, MSG_WAIT, 0);
  sprintf(msg, "%d / %d セクタ完了", write_sector, in_sec_num);
  msg_win(msg, 0, MSG_LINE, 1);

  sceRtcGetCurrentTick(&start_tick);

  set_clock(333, 166);

  // 2ブロック分先行読込
  read_size[0] = sceIoRead(fp_in, read_buf[0], MAX_READ_SIZE);
  ERR_RET_2(read_size[0], ERR_READ);
  read_sector += read_size[0];
  read_size[1] = sceIoReadAsync(fp_in, read_buf[1], MAX_READ_SIZE);
  ERR_RET_2(read_size[1], ERR_READ);

  // loop前
  write_sector = 0;
  num = 0;

  // loop
  while(in_sec_num > write_sector)
  {
    // 遅延書込み終了
    msg_win("Wait WRITE", 0, MSG_LINE, 4);
    ret = sceIoWaitAsync(fp_out, &res);
    if(res < 0)
    {
      if(first_wait == 1)
      {
        err_msg(ERR_WRITE);
        return CANCEL;
      }
      first_wait = 1;
    }

    write_sector += read_size[num];

    // 遅延書込み開始
    write_size = read_size[num] << 11;
    msg_win("WRITE", 0, MSG_LINE, 4);
    ret = sceIoWriteAsync(fp_out, write_buf[num], write_size);
    ERR_RET_2(ret, ERR_WRITE);

    // 遅延読込み終了
    if(read_sector < in_sec_num)
    {
      msg_win("Wait READ", 0, MSG_LINE, 4);
      ret = sceIoWaitAsync(fp_in, &res);
      ERR_RET_2(res, ERR_READ);
      read_size[(num + 1) % 3] = res;
      read_sector += res;
    }
    // 遅延読込み開始
    if(read_sector < in_sec_num)
    {
      msg_win("READ", 0, MSG_LINE, 4);
      ret = sceIoReadAsync(fp_in, read_buf[(num + 2) % 3], MAX_READ_SIZE);
      if(ret < 0)
      {
        err_msg(ERR_READ);
        return CANCEL;
      }
    }

    sprintf(msg, "%d / %d セクタ完了", write_sector, in_sec_num);
    msg_win(msg, 0, MSG_LINE, 1);
    sceRtcGetCurrentTick(&now_tick);
    now_tick -= start_tick;
    ret = (MAX_READ_SIZE << 11) / 1024 * 1000 /((now_tick - old_tick) / 1000);
    old_tick = now_tick;
    sceRtcSetTick(&date1, &now_tick);
    now_tick = now_tick * in_sec_num / write_sector;
    sceRtcSetTick(&date2, &now_tick);
    now_tick -= old_tick;
    sceRtcSetTick(&date3, &now_tick);
    sprintf(msg, "経過 %02d:%02d:%02d / 予想 %02d:%02d:%02d / 残 %02d:%02d:%02d",
        date1.hour, date1.minutes, date1.seconds,
        date2.hour, date2.minutes, date2.seconds,
        date3.hour, date3.minutes, date3.seconds);
    msg_win(msg, 0, MSG_LINE, 2);
    sprintf(msg, "速度 %4dKB/s", ret);
    msg_win(msg, 0, MSG_LINE, 3);

    get_button(&data);
    if((data.Buttons) == PSP_CTRL_CROSS)
    {
      msg_win("STOP !!", 0, MSG_LINE, 2);
      ret = select_menu("中止しますか？", select_no_yes, 0, 28, 10);
      if(ret == 1)
      {
        text[0] = "変換中止";
        text[1] = "◎を押してください";
        text[2] = "\0";
        dialog(text);
        ret = -1;
        goto LABEL_ERR;
      }
      dir_menu(NULL, YES);
      msg_win("", 0, MSG_REDROW, 0);
    }
    num = (num + 1) % 3;
  }

  // msg
  sprintf(msg, "%d / %d セクタ完了", in_sec_num, in_sec_num);
  msg_win(msg, 0, MSG_LINE, 1);

  sceRtcGetCurrentTick(&now_tick);
  now_tick -= start_tick;
  sceRtcSetTick(&date3, &now_tick);
  ret = (in_sec_num * SECTOR_SIZE) / 1024 * 1000 / (now_tick / 1000);
  sprintf(msg, "変換時間 %02d:%02d:%02d / 平均速度 %04dKB/s", date3.hour, date3.minutes, date3.seconds, ret);
  msg_win(msg, 0, MSG_LINE, 0);
  sprintf(msg, "経過 %02d:%02d:%02d / 予想 %02d:%02d:%02d / 残 00:00:00",
      date1.hour, date1.minutes, date1.seconds,
      date2.hour, date2.minutes, date2.seconds);
  msg_win(msg, 0, MSG_LINE, 2);
  msg_win("", 0, MSG_LINE, 3);
  msg_win("", 0, MSG_LINE, 4);
  msg_win("", 0, MSG_LINE, 5);
  ret = 0;

LABEL_ERR:
  // close
  sceIoWaitAsync(fp_in, &res);
  sceIoWaitAsync(fp_out, &res);
  sceIoClose(fp_in);
  sceIoClose(fp_out);

  if(ret != -1)
  {
    sceIoRemove(out_path);
    sceIoRename(ren_path, out_path);

    text[0] = "変換完了";
    text[1] = "◎を押してください";
    text[2] = "\0";
    dialog(text);
  }
  else
    sceIoRemove(ren_path);

  set_clock(222, 111);
  return DONE;
}

int umd2cso(char *dir, char *file, file_type type, int opt_1, int opt_2)
{
  char in_path[MAX_PATH];
  char out_path[MAX_PATH];
  char ren_path[MAX_PATH];
  char work[MAX_PATH];
  char work2[MAX_PATH];
  char *read_buf[3];                // 読込バッファ ポインタ
  char *write_buf[3];               // 書込バッファ ポインタ

  SceUID fp_in = 0;                 // 読込ファイル ポインタ
  SceUID fp_out = 0;                // 書込ファイル ポインタ
  SceInt64 res = -1;                // 非同期ファイルIO用

  const int MAX_READ_SIZE = 512;    // 一回の読込サイズ

  int in_sec_num = 0;               // 総セクタ数
  int read_size[3];                 // 読込んだサイズ
  int write_size = 0;               // 一回の書込サイズ
  int write_sector = 0;             // 処理したセクタ数
  int read_sector = 0;              // 処理したセクタ数
  int cso_sector = 0;              // 処理したセクタ数

  int num;                          // 汎用
  int ret;
  int loop;

  char *text[15];                   // テキスト表示用
  char msg[256];                    // テキスト表示用

  SceCtrlData  data;
  u64 start_tick;
  u64 now_tick;
  u64 old_tick = 0;
  pspTime date1;
  pspTime date2;
  pspTime date3;
  int first_wait = 0;

  CISO_H header;            // CSOヘッダ
  int *tag = NULL;          // CSOセクタタグ ポインタ
  int tag_size = 0;         // CSOセクタタグ サイズ
  char cso_buf[0x1000];     // CSO変換バッファ
  int write_ptr = 0;        // 書込バッファへの転送位置
  int cso_sec_ptr = 0;          // CSOセクタ位置
  int cso_sec_size;         // 圧縮後のセクタサイズ

  // buff 設定
  read_buf[0] = &WORK[O_BUFFER][0];
  read_buf[1] = &WORK[O_BUFFER + MAX_READ_SIZE][0];
  read_buf[2] = &WORK[O_BUFFER + MAX_READ_SIZE * 2][0];
  write_buf[0] = &WORK[O_BUFFER + MAX_READ_SIZE * 3][0];
  write_buf[1] = &WORK[O_BUFFER + MAX_READ_SIZE * 4][0];
  write_buf[2] = &WORK[O_BUFFER + MAX_READ_SIZE * 5][0];

  // tag設定
  tag = (int *)&WORK[O_BUFFER + MAX_READ_SIZE * 6][0];

  // MSチェック
  ret = check_ms();
  if(ret < 0)
    return CANCEL;

  // UMDチェック
  ret = check_umd();
  if(ret < 0)
    return CANCEL;

  // 空き容量チェック
  if((global.umd_size / 1024) > global.ms_free_size)
  {
    msg_win("空き容量がUMDサイズ以下です", 0, MSG_WAIT, 0);
    msg_win("途中で変換に失敗する可能性があります", 0, MSG_WAIT, 0);
    ret = select_menu("続行しますか？", select_yes_no, 0, 28, 10);
    if(ret != YES)
      return CANCEL;
  }

  // パラメータ設定
  msg_win("", 0, MSG_CLEAR, 0);
  ret = select_menu_list(cso_trans_menu);
  if(ret == CANCEL )
    return CANCEL;

  // ファイル名設定
  strcpy(in_path, dir);
  strcat(in_path, file);

  strcpy(out_path, dir);
  strcpy(work2, global.umd_id);
  sprintf(work, "_%d_%03d", global.cso_level, global.cso_threshold);
  strcat(work2, work);

  ret = osk(work, work2, "ファイル名指定", 0);
  if(ret == PSP_UTILITY_OSK_RESULT_CHANGED)
    strcat(out_path, work);
  else
    strcat(out_path, work2);

  strcat(out_path, ".CSO");

  strcpy(ren_path, dir);
  strcat(ren_path, "01234565789012345678901234567890123456789012345678901234567890123456789");

  msg_win("", 0, MSG_CLEAR, 0);
  sjis_to_utf8(work, strrchr(out_path, '/') + 1);
  sprintf(msg, "出力名：%s", work);
  msg_win(msg, 0, MSG_WAIT, 0);

  if(check_file(out_path) < 0)
  {
    msg_win("開始しますか？", 0, MSG_WAIT, 0);
    ret = select_menu("開始", select_yes_no, 0, 28, 10);
  }
  else
  {
    msg_win("同名のファイルがあります", 0, MSG_WAIT, 0);
    msg_win("上書きしますか？", 0, MSG_WAIT, 0);
    ret = select_menu("上書き", select_no_yes, 0, 28, 10);
  }

  if(ret != YES)
    return CANCEL;

  in_sec_num = get_umd_sector("umd:", TYPE_UMD);
  tag_size = in_sec_num + 1;

  // open設定
  fp_in  = sceIoOpen("umd:", PSP_O_RDONLY, 0777);
  ERR_RET_2(fp_in, ERR_OPEN);
  sceIoLseek32(fp_in, 0, SEEK_SET);

  fp_out = sceIoOpen(ren_path, PSP_O_WRONLY | PSP_O_CREAT | PSP_O_TRUNC, 0777);
  ERR_RET_2(fp_out, ERR_OPEN);
  sceIoLseek32(fp_out, 0, SEEK_SET);

  // msg
  msg_win("", 0, MSG_CLEAR, 0);
  sprintf(msg, "UMD -> CSO 変換 (圧縮レベル:%d/閾値%3d%%)", global.cso_level, global.cso_threshold);
  msg_win(msg, 0, MSG_WAIT, 0);
  sprintf(msg, "%d / %d セクタ完了", write_sector, in_sec_num);
  msg_win(msg, 0, MSG_LINE, 1);

  sceRtcGetCurrentTick(&start_tick);

  set_clock(333, 166);

  // 2ブロック分先行読込
  read_size[0] = sceIoRead(fp_in, read_buf[0], MAX_READ_SIZE);
  ERR_RET_2(read_size[0], ERR_READ);
  read_sector += read_size[0];
  read_size[1] = sceIoReadAsync(fp_in, read_buf[1], MAX_READ_SIZE);
  ERR_RET_2(read_size[1], ERR_READ);

  // header
  header.magic[0] = 'C';
  header.magic[1] = 'I';
  header.magic[2] = 'S';
  header.magic[3] = 'O';
  header.header_size = 0x00; /* 本来 0x18 であるが、PSP Filer 6.6で読み込めなくなるので一時的に修正*/
  header.total_bytes = in_sec_num * SECTOR_SIZE;
  header.block_size = SECTOR_SIZE;
  header.ver = 0x01;
  header.align = 0x00;
  header.rsv_06[0] = 0x00;
  header.rsv_06[1] = 0x00;

  // ヘッダ書込み
  ret = sceIoWrite(fp_out, &header, CISO_HEADER_SIZE);
  ERR_RET_2(ret, ERR_WRITE);
  cso_sec_ptr = CISO_HEADER_SIZE + (tag_size * 4);
  tag[0] = cso_sec_ptr;
  ret = sceIoLseek32(fp_out, cso_sec_ptr, PSP_SEEK_SET);
  ERR_RET_2(ret, ERR_SEEK);

  // loop前
  cso_sector = 0;
  write_sector = 0;
  num = 0;

  // loop
  while(in_sec_num > write_sector)
  {
    write_sector += read_size[num];

    // CSO変換
    msg_win("DEFLATE", 0, MSG_LINE, 4);
    loop = 0;
    write_size = 0;
    write_ptr = 0;
    while(read_size[num] > 0)
    {
      cso_sec_size = deflate_cso(cso_buf, sizeof(cso_buf), (read_buf[num] + (loop * SECTOR_SIZE)), SECTOR_SIZE, global.cso_level);
      if(cso_sec_size < 0)
      {
        text[0] = "CSO変換エラー";
        text[1] = "◎を押してください";
        text[2] = "\0";
        dialog(text);
        ret = -1;
        goto LABEL_ERR;
      }
      if(cso_sec_size < (SECTOR_SIZE * global.cso_threshold / 100))
      {
        write_size += cso_sec_size;
        cso_sec_ptr += cso_sec_size;
        tag[cso_sector + 1] = cso_sec_ptr;
        memcpy(write_buf[num] + write_ptr, cso_buf, cso_sec_size);
      }
      else
      {
        cso_sec_size = SECTOR_SIZE;
        write_size += SECTOR_SIZE;
        cso_sec_ptr += SECTOR_SIZE;
        tag[cso_sector] |= 0x80000000;
        tag[cso_sector + 1] = cso_sec_ptr;
        memcpy(write_buf[num] + write_ptr, read_buf[num] + loop * SECTOR_SIZE, SECTOR_SIZE);
      }
      write_ptr += cso_sec_size;
      read_size[num]--;
      loop++;
      cso_sector++;
    }

    // 遅延書込み終了
    msg_win("Wait WRITE", 0, MSG_LINE, 4);
    ret = sceIoWaitAsync(fp_out, &res);
    if(res < 0)
    {
      if(first_wait == 1)
      {
        err_msg(ERR_WRITE);
        return CANCEL;
      }
      first_wait = 1;
    }

    // 遅延書込み開始
    msg_win("WRITE", 0, MSG_LINE, 4);
    ret = sceIoWriteAsync(fp_out, write_buf[num], write_size);
    ERR_RET_2(ret, ERR_WRITE);

    // 遅延読込み終了
    if(read_sector < in_sec_num)
    {
      msg_win("Wait READ", 0, MSG_LINE, 4);
      ret = sceIoWaitAsync(fp_in, &res);
      ERR_RET_2(res, ERR_READ);
      read_size[(num + 1) % 3] = res;
      read_sector += res;
    }
    // 遅延読込み開始
    if(read_sector < in_sec_num)
    {
      msg_win("READ", 0, MSG_LINE, 4);
      ret = sceIoReadAsync(fp_in, read_buf[(num + 2) % 3], MAX_READ_SIZE);
      if(ret < 0)
      {
        err_msg(ERR_READ);
        return CANCEL;
      }
    }

    sprintf(msg, "%d / %d セクタ完了", write_sector, in_sec_num);
    msg_win(msg, 0, MSG_LINE, 1);
    sceRtcGetCurrentTick(&now_tick);
    now_tick -= start_tick;
    ret = (MAX_READ_SIZE << 11) / 1024 * 1000 /((now_tick - old_tick) / 1000);
    old_tick = now_tick;
    sceRtcSetTick(&date1, &now_tick);
    now_tick = now_tick * in_sec_num / write_sector;
    sceRtcSetTick(&date2, &now_tick);
    now_tick -= old_tick;
    sceRtcSetTick(&date3, &now_tick);
    sprintf(msg, "経過 %02d:%02d:%02d / 予想 %02d:%02d:%02d / 残 %02d:%02d:%02d",
        date1.hour, date1.minutes, date1.seconds,
        date2.hour, date2.minutes, date2.seconds,
        date3.hour, date3.minutes, date3.seconds);
    msg_win(msg, 0, MSG_LINE, 2);
    sprintf(msg, "速度 %4dKB/s", ret);
    msg_win(msg, 0, MSG_LINE, 3);

    get_button(&data);
    if((data.Buttons) == PSP_CTRL_CROSS)
    {
      msg_win("STOP !!", 0, MSG_LINE, 2);
      ret = select_menu("中止しますか？", select_no_yes, 0, 28, 10);
      if(ret == 1)
      {
        text[0] = "変換中止";
        text[1] = "◎を押してください";
        text[2] = "\0";
        dialog(text);
        ret = -1;
        goto LABEL_ERR;
      }
      dir_menu(NULL, YES);
      msg_win("", 0, MSG_REDROW, 0);
    }
    num = (num + 1) % 3;
  }

  // msg
  sprintf(msg, "%d / %d セクタ完了", in_sec_num, in_sec_num);
  msg_win(msg, 0, MSG_LINE, 1);

  sceRtcGetCurrentTick(&now_tick);
  now_tick -= start_tick;
  sceRtcSetTick(&date3, &now_tick);
  ret = (in_sec_num * SECTOR_SIZE) / 1024 * 1000 / (now_tick / 1000);
  sprintf(msg, "変換時間 %02d:%02d:%02d / 平均速度 %04dKB/s", date3.hour, date3.minutes, date3.seconds, ret);
  msg_win(msg, 0, MSG_LINE, 0);
  sprintf(msg, "経過 %02d:%02d:%02d / 予想 %02d:%02d:%02d / 残 00:00:00",
      date1.hour, date1.minutes, date1.seconds,
      date2.hour, date2.minutes, date2.seconds);
  msg_win(msg, 0, MSG_LINE, 2);
  msg_win("", 0, MSG_LINE, 3);
  msg_win("", 0, MSG_LINE, 4);
  msg_win("", 0, MSG_LINE, 5);
  ret = 0;

LABEL_ERR:
  // close
  sceIoWaitAsync(fp_in, &res);
  sceIoWaitAsync(fp_out, &res);

  // header
  ret = sceIoLseek32(fp_out, CISO_HEADER_SIZE, PSP_SEEK_SET);
  ERR_RET_2(ret, ERR_SEEK);
  ret = sceIoWrite(fp_out, tag, tag_size * sizeof(unsigned int));
  ERR_RET_2(ret, ERR_WRITE);

  sceIoClose(fp_in);
  sceIoClose(fp_out);

  if(ret != -1)
  {
    sceIoRemove(out_path);
    sceIoRename(ren_path, out_path);

    text[0] = "変換完了";
    text[1] = "◎を押してください";
    text[2] = "\0";
    dialog(text);
  }
  else
    sceIoRemove(ren_path);

  set_clock(222, 111);
  return DONE;
}

int iso2cso(char *dir, char *file, file_type type, int opt_1, int opt_2)
{
  char iso_path[MAX_PATH];
  char cso_path[MAX_PATH];
  char work[MAX_PATH];
  char work2[MAX_PATH];
  char work3[MAX_PATH];
  int ret;
  char *ptr;

  ret = check_ms();
  if(ret < 0)
    return CANCEL;

  if((global.umd_size / 1024) > global.ms_free_size)
  {
    msg_win("空き容量が元のISOサイズ以下です", 0, MSG_WAIT, 0);
    msg_win("途中で変換に失敗する可能性があります", 0, MSG_WAIT, 0);
    ret = select_menu("続行しますか？", select_yes_no, 0, 28, 10);
    if(ret != YES)
      return CANCEL;
  }

  msg_win("", 0, MSG_CLEAR, 0);
  ret = select_menu_list(cso_trans_menu);
  if(ret == CANCEL)
    return CANCEL;

  strcpy(iso_path, dir);
  strcat(iso_path, file);

  strcpy(cso_path, dir);
  strcpy(work3, file);
  ptr = strrchr(work3, '.');
  *ptr = '\0';
  sprintf(work, "_%d_%03d", global.cso_level, global.cso_threshold);
  strcat(work3, work);

  sjis_to_utf8(work2, work3);

  ret = osk(work, work2, "ファイル名指定", 0);
  if(ret == PSP_UTILITY_OSK_RESULT_CHANGED)
    strcat(cso_path, work);
  else
    strcat(cso_path, work3);

  strcat(cso_path, ".CSO");

  set_clock(333, 166);
  file_trans(cso_path, iso_path, TRANS_ISO_CSO, 0, 0, opt_1, opt_2);
  set_clock(222, 111);
  return DONE;
}

int cso2iso(char *dir, char *file, file_type type, int opt_1, int opt_2)
{
  char iso_path[MAX_PATH];
  char cso_path[MAX_PATH];
  char work[MAX_PATH];
  char work2[MAX_PATH];
  char work3[MAX_PATH];
  int ret;
  char *ptr;

  ret = check_ms();
  if(ret < 0)
    return CANCEL;

  if((global.umd_size / 1024) > global.ms_free_size)
  {
    err_msg(ERR_SIZE_OVER);
    return CANCEL;
  }

  strcpy(cso_path, dir);
  strcat(cso_path, file);

  strcpy(iso_path, dir);
  strcpy(work3, file);
  ptr = strrchr(work3, '.');
  *ptr = '\0';
  sjis_to_utf8(work2, work3);

  ret = osk(work, work2, "ファイル名指定", 0);
  if(ret == PSP_UTILITY_OSK_RESULT_CHANGED)
    strcat(iso_path, work);
  else
    strcat(iso_path, work3);
  strcat(iso_path, ".ISO");

  set_clock(333, 166);
  file_trans(iso_path, cso_path, TRANS_CSO_ISO, 0, 0, opt_1, opt_2);
  set_clock(222, 111);
  return DONE;
}

int file_trans(char* out_path, char *in_path, trans_type type, int level, int limit, int opt_1, int opt_2)
{
  char *read_buf[2];        // 読込バッファ ポインタ
  char *write_buf[2];       // 書込バッファ ポインタ

  SceUID fp_in = 0;         // 読込ファイル ポインタ
  SceUID fp_out = 0;        // 書込ファイル ポインタ
  SceIoStat stat;           // ファイルステータス
  SceInt64 res = -1;        // 非同期ファイルIO用

  CISO_H header;            // CSOヘッダ
  int *tag = NULL;          // CSOセクタタグ ポインタ
  int tag_size = 0;         // CSOセクタタグ サイズ
  char cso_buf[0x1000];     // CSO変換バッファ
  int write_ptr = 0;        // 書込バッファへの転送位置
  int cso_sec_ptr = 0;          // CSOセクタ位置
  int cso_sec_size;         // 圧縮後のセクタサイズ
  int out_cso_flag = 0;

  int in_sec_num = 0;       // 読込セクタ数

  int max_read_size = 0;    // 一回の読込サイズ
  int read_size = 0;        // 読込んだサイズ
  int write_size = 0;       // 一回の書込サイズ
  int now_sector = 0;       // 処理したセクタ数
  int write_block_shift = 0;    // 処理単位(シフト数)
  int read_block_shift = 0;    // 処理単位(シフト数)

  int num;                  // 汎用
  int loop;                 // ループ用

  char *text[15];           // テキスト表示用
  char msg[256];            // テキスト表示用
  char work[256];            // テキスト表示用
  char ren_path[MAX_PATH];

  int ret;
  SceCtrlData  data;
  u64 start_tick;
  u64 now_tick;
  u64 old_tick = 0;
  pspTime date1;
  pspTime date2;
  pspTime date3;
  int first_wait = 0;

  strcpy(ren_path, out_path);
  strcpy(&ren_path[strlen(out_path) - 3], "TMP");

  msg_win("", 0, MSG_CLEAR, 0);
  sjis_to_utf8(work, strrchr(out_path, '/') + 1);
  sprintf(msg, "出力名：%s", work);
  msg_win(msg, 0, MSG_WAIT, 0);

  if(check_file(out_path) < 0)
  {
    msg_win("開始しますか？", 0, MSG_WAIT, 0);
    ret = select_menu("開始", select_yes_no, 0, 28, 10);
  }
  else
  {
    msg_win("同名のファイルがあります", 0, MSG_WAIT, 0);
    msg_win("上書きしますか？", 0, MSG_WAIT, 0);
    ret = select_menu("上書き", select_no_yes, 0, 28, 10);
  }

  if(ret != YES)
    return CANCEL;

  // buff 設定
  switch(type)
  {
    case TRANS_UMD_ISO:
      strcpy(msg, "UMD -> ISO 変換");

      // buff 設定
      read_buf[0] = &WORK[O_BUFFER][0];
      read_buf[1] = &WORK[I_BUFFER][0];
      write_buf[0] = &WORK[O_BUFFER][0];
      write_buf[1] = &WORK[I_BUFFER][0];

      // tag設定
      out_cso_flag = 0;

      // block設定
      max_read_size = 512;
      write_block_shift = 11; /* SECTOR_SIZE */
      read_block_shift = 0;

      in_sec_num = get_umd_sector("umd:", TYPE_UMD);
      tag_size = 0;

      // open設定
      fp_in  = sceIoOpen("umd:", PSP_O_RDONLY, 0777);
      ERR_RET_2(fp_in, ERR_OPEN);
      fp_out = sceIoOpen(ren_path, PSP_O_WRONLY | PSP_O_CREAT | PSP_O_TRUNC, 0777);
      ERR_RET_2(fp_out, ERR_OPEN);

      break;

    case TRANS_UMD_CSO:
      sprintf(msg, "UMD -> CSO 変換 (圧縮レベル:%d/閾値%3d%%)", global.cso_level, global.cso_threshold);

      // buff 設定
      read_buf[0] = &WORK[O_BUFFER][0];
      read_buf[1] = &WORK[O_BUFFER + 2048][0];
      write_buf[0] = &WORK[O_BUFFER + 4096][0];
      write_buf[1] = &WORK[O_BUFFER + 6144][0];

      // tag設定
      tag = (int *)&WORK[O_BUFFER + 8192][0];
      out_cso_flag = 1;

      // block設定
      max_read_size = 512;
      write_block_shift = 11;
      read_block_shift = 0;

      in_sec_num = get_umd_sector("umd:", TYPE_UMD);
      tag_size = in_sec_num + 1;

      // open設定
      fp_in  = sceIoOpen("umd:", PSP_O_RDONLY, 0777);
      ERR_RET_2(fp_in, ERR_OPEN);
      fp_out = sceIoOpen(ren_path, PSP_O_WRONLY | PSP_O_CREAT | PSP_O_TRUNC, 0777);
      ERR_RET_2(fp_out, ERR_OPEN);

      break;

    case TRANS_ISO_CSO:
      sprintf(msg, "ISO -> CSO 変換 (圧縮レベル: %d / 閾値 %3d%%)", global.cso_level, global.cso_threshold);

      // buff 設定
      read_buf[0] = &WORK[O_BUFFER][0];
      read_buf[1] = &WORK[O_BUFFER + 2048][0];
      write_buf[0] = &WORK[O_BUFFER + 4096][0];
      write_buf[1] = &WORK[O_BUFFER + 6144][0];

      // tag設定
      tag = (int *)&WORK[O_BUFFER + 8192][0]; // 最大4MB
      out_cso_flag = 1;

      // block設定
      max_read_size = 2048 * SECTOR_SIZE;
      write_block_shift = 0;
      read_block_shift = 11;

      ret = sceIoGetstat(in_path, &stat);
      ERR_RET_2(ret, ERR_READ);
      in_sec_num = (stat.st_size + SECTOR_SIZE - 1) / SECTOR_SIZE;
      tag_size = in_sec_num + 1;

      // open設定
      fp_in  = sceIoOpen(in_path, PSP_O_RDONLY, 0777);
      ERR_RET_2(fp_in, ERR_OPEN);
      fp_out = sceIoOpen(ren_path, PSP_O_WRONLY | PSP_O_CREAT | PSP_O_TRUNC, 0777);
      ERR_RET_2(fp_out, ERR_OPEN);

      break;

    case TRANS_CSO_ISO:
      strcpy(msg, "CSO -> ISO 変換");

      // buff 設定
      read_buf[0] = &WORK[I_BUFFER][0];
      read_buf[1] = &WORK[O_BUFFER][0];
      write_buf[0] = &WORK[I_BUFFER][0];
      write_buf[1] = &WORK[O_BUFFER][0];

      // tag設定
      out_cso_flag = 0;

      // block設定
      max_read_size = 2048 * SECTOR_SIZE;
      write_block_shift = 0;
      read_block_shift = 11;

      tag_size = 0;

      // open設定
      fp_in  = sceIoOpen(in_path, PSP_O_RDONLY, 0777);
      ERR_RET_2(fp_in, ERR_OPEN);
      fp_out = sceIoOpen(ren_path, PSP_O_WRONLY | PSP_O_CREAT | PSP_O_TRUNC, 0777);
      ERR_RET_2(fp_out, ERR_OPEN);

      ret = sceIoRead(fp_in, &header, CISO_HEADER_SIZE);
      ERR_RET_2(ret, ERR_READ);
      in_sec_num = header.total_bytes / SECTOR_SIZE;

      break;

    default:
      // buff 設定
      // tag設定
      // block設定
      // open設定
      return DONE;
      break;
  }

  // msg
  msg_win("", 0, MSG_CLEAR, 0);
  msg_win(msg, 1, MSG_WAIT, 1);

  if(out_cso_flag == 1)
  {
    // header
    header.magic[0] = 'C';
    header.magic[1] = 'I';
    header.magic[2] = 'S';
    header.magic[3] = 'O';
    header.header_size = 0x00; /* 本来 0x18 であるが、PSP Filer 6.6で読み込めなくなるので一時的に修正*/
    header.total_bytes = in_sec_num * SECTOR_SIZE;
    header.block_size = SECTOR_SIZE;
    header.ver = 0x01;
    header.align = 0x00;
    header.rsv_06[0] = 0x00;
    header.rsv_06[1] = 0x00;

    ret = sceIoWrite(fp_out, &header, CISO_HEADER_SIZE);
    ERR_RET_2(ret, ERR_WRITE);
    cso_sec_ptr = CISO_HEADER_SIZE + (tag_size * 4);
    tag[0] = cso_sec_ptr;
    ret = sceIoLseek32(fp_out, cso_sec_ptr, PSP_SEEK_SET);
    ERR_RET_2(ret, ERR_SEEK);
  }


  // loop前
  now_sector = 0;
  num = 0;

  sceRtcGetCurrentTick(&start_tick);

  // msg
  msg_win("", 0, MSG_WAIT, 0);
  sprintf(msg, "%d / %d セクタ完了", now_sector, in_sec_num);
  msg_win(msg, 0, MSG_LINE, 1);

  if(type == TRANS_CSO_ISO)
    read_size = cso_read_fp(read_buf[0], fp_in, now_sector << read_block_shift, max_read_size);
  else
  {
    read_size = sceIoRead(fp_in, read_buf[0], max_read_size);
    ERR_RET_2(read_size, ERR_READ);
  }

  // loop
  while(read_size > 0)
  {
    if(out_cso_flag == 1)
    {
      msg_win("DEFLATE", 0, MSG_LINE, 4);
      loop = 0;
      write_size = 0;
      write_ptr = 0;
      while(read_size > 0)
      {
        cso_sec_size = deflate_cso(cso_buf, sizeof(cso_buf), (read_buf[num] + (loop * SECTOR_SIZE)), SECTOR_SIZE, global.cso_level);
        if(cso_sec_size < 0)
        {
          text[0] = "CSO変換エラー";
          text[1] = "◎を押してください";
          text[2] = "\0";
          dialog(text);
          ret = -1;
          goto LABEL_ERR;
        }
        if(cso_sec_size < (SECTOR_SIZE * global.cso_threshold / 100))
        {
          write_size += cso_sec_size;
          cso_sec_ptr += cso_sec_size;
          tag[now_sector + 1] = cso_sec_ptr;
          memcpy(write_buf[num] + write_ptr, cso_buf, cso_sec_size);
        }
        else
        {
          cso_sec_size = SECTOR_SIZE;
          write_size += SECTOR_SIZE;
          cso_sec_ptr += SECTOR_SIZE;
          tag[now_sector] |= 0x80000000;
          tag[now_sector + 1] = cso_sec_ptr;
          memcpy(write_buf[num] + write_ptr, read_buf[num] + loop * SECTOR_SIZE, SECTOR_SIZE);
        }
        write_ptr += cso_sec_size;
        read_size -= SECTOR_SIZE >> write_block_shift;
        loop++;
        now_sector++;
      }
    }
    else
    {
      now_sector += max_read_size >> read_block_shift;
      write_size = read_size << write_block_shift;
    }

    msg_win("Wait WRITE", 0, MSG_LINE, 4);
    ret = sceIoWaitAsync(fp_out, &res);
    if(ret < 0)
    {
      if(first_wait == 1)
      {
        err_msg(ERR_WRITE);
        return CANCEL;
      }
      first_wait = 1;
    }

    if(type != TRANS_CSO_ISO)
    {
      msg_win("READ", 0, MSG_LINE, 4);
      ret = sceIoReadAsync(fp_in, read_buf[(num + 1) % 2], max_read_size);
      if(ret < 0)
      {
        err_msg(ERR_READ);
        return CANCEL;
      }
    }

    msg_win("WRITE", 0, MSG_LINE, 4);
    ret = sceIoWriteAsync(fp_out, write_buf[num], write_size);
    ERR_RET_2(ret, ERR_WRITE);

    num = (num + 1) % 2;
    sprintf(msg, "%d / %d セクタ完了", now_sector, in_sec_num);
    msg_win(msg, 0, MSG_LINE, 1);
    sceRtcGetCurrentTick(&now_tick);
    now_tick -= start_tick;
    ret = (max_read_size << write_block_shift) / 1024 * 1000 / ((now_tick - old_tick) / 1000);
    old_tick = now_tick;
    sceRtcSetTick(&date1, &now_tick);
    now_tick = now_tick * in_sec_num / now_sector;
    sceRtcSetTick(&date2, &now_tick);
    now_tick -= old_tick;
    sceRtcSetTick(&date3, &now_tick);
    sprintf(msg, "経過 %02d:%02d:%02d / 予想 %02d:%02d:%02d / 残 %02d:%02d:%02d",
        date1.hour, date1.minutes, date1.seconds,
        date2.hour, date2.minutes, date2.seconds,
        date3.hour, date3.minutes, date3.seconds);
    msg_win(msg, 0, MSG_LINE, 2);
    sprintf(msg, "速度 %4dKB/s", ret);
    msg_win(msg, 0, MSG_LINE, 3);

    get_button(&data);
    if((data.Buttons) == PSP_CTRL_CROSS)
    {
      msg_win("STOP !!", 0, MSG_LINE, 2);
      ret = select_menu("中止しますか？", select_no_yes, 0, 28, 10);
      if(ret == 1)
      {
        text[0] = "変換中止";
        text[1] = "◎を押してください";
        text[2] = "\0";
        dialog(text);
        ret = -1;
        goto LABEL_ERR;
      }
      dir_menu(NULL, YES);
      msg_win("", 0, MSG_REDROW, 0);
    }

    msg_win("Wait READ", 0, MSG_LINE, 4);
    if(type == TRANS_CSO_ISO)
      res = cso_read_fp(read_buf[num], fp_in, now_sector << read_block_shift, max_read_size);
    else
    {
      ret = sceIoWaitAsync(fp_in, &res);
      ERR_RET_2(ret, ERR_READ);
      msg_win("READ", 0, MSG_LINE, 4);
    }
    read_size = res;
  }

  // msg
  sprintf(msg, "%d / %d セクタ完了", in_sec_num, in_sec_num);
  msg_win(msg, 0, MSG_LINE, 1);

  sceRtcGetCurrentTick(&now_tick);
  now_tick -= start_tick;
  sceRtcSetTick(&date3, &now_tick);
  ret = (in_sec_num * SECTOR_SIZE) / 1024 * 1000 / (now_tick / 1000);
  sprintf(msg, "変換時間 %02d:%02d:%02d / 平均速度 %04dKB/s", date3.hour, date3.minutes, date3.seconds, ret);
  msg_win(msg, 0, MSG_LINE, 0);
  sprintf(msg, "経過 %02d:%02d:%02d / 予想 %02d:%02d:%02d / 残 00:00:00",
      date1.hour, date1.minutes, date1.seconds,
      date2.hour, date2.minutes, date2.seconds);
  msg_win(msg, 0, MSG_LINE, 2);

  // header
  if(out_cso_flag == 1)
  {
    ret = sceIoWaitAsync(fp_out, &res);
    ERR_RET_2(ret, ERR_READ);
    ret = sceIoLseek32(fp_out, CISO_HEADER_SIZE, PSP_SEEK_SET);
    ERR_RET_2(ret, ERR_SEEK);
    ret = sceIoWriteAsync(fp_out, tag, tag_size * sizeof(unsigned int));
    ERR_RET_2(ret, ERR_WRITE);
  }

LABEL_ERR:
  // close
  sceIoWaitAsync(fp_in, &res);
  sceIoWaitAsync(fp_out, &res);
  sceIoClose(fp_in);
  sceIoClose(fp_out);

  if(ret != -1)
  {
    sceIoRename(ren_path, out_path);

    text[0] = "変換完了";
    text[1] = "◎を押してください";
    text[2] = "\0";
    dialog(text);
  }
  else
  {
    sceIoRemove(ren_path);
  }

  return DONE;
}

// TODO 別関数にまとめる
/*---------------------------------------------------------------------------
---------------------------------------------------------------------------*/
int file_update(char *dir, char *file, file_type type, int opt_1, int opt_2)
{
  char *msg[2] = { "UMD_ID.csvを更新しますか？", "iso_toolを更新しますか？" };
  char *url[2] = { "http://isotool.tfact.net/UMD_ID.csv", "http://isotool.tfact.net/EBOOT.PBP" };
  char old_path[MAX_PATH];
  char new_path[MAX_PATH];
  time_t old_time;
  time_t new_time;
  int tz;
  SceIoStat old_stat;
  int ret;
  char *text[15];           // テキスト表示用
  int mod = 0;

  msg_win("", 0, MSG_CLEAR, 0);
  msg_win(msg[opt_2], 1, MSG_WAIT, 1);
  ret = select_menu("確認", select_no_yes, 0, 25, 8);

  if(ret < 0)
    return -1;

  if(ret == 1)
  {
    // 接続
    msg_win("接続", 0, MSG_WAIT, 0);
    net_connect();

    msg_win("更新確認", 0, MSG_WAIT, 0);
    // パス設定
    strcpy(old_path, main_path);
    strcat(old_path, "/");
    strcat(old_path, (char *)opt_1);

    strcpy(new_path, old_path);
    strcat(new_path, ".new");

    sceUtilityGetSystemParamInt(PSP_SYSTEMPARAM_ID_INT_TIMEZONE, &tz);

    // 旧ファイルの時刻取得
    ret = sceIoGetstat(old_path, &old_stat);
    if(ret < 0)
      old_time = 0;
    else
    {
      sceRtcGetTime_t((pspTime*)&old_stat.st_mtime, &old_time);
      old_time -= tz * 60; // local -> UTC
    }
    // 新ファイルの時刻取得
    ret = web_get_file_time(&new_time, url[opt_2]);
    // 取得エラー、更新なしの場合はメッセージを表示して終了
    if(ret < 0)
      msg_win("更新時刻取得エラー", 1, MSG_WAIT, 0);
    else if(new_time < (old_time + 60)) // 誤差を考えて1分以内の変更は変更なしとする
      msg_win("更新されていません", 1, MSG_WAIT, 0);
    else
    {
      msg_win("更新が見つかりました。更新しますか？", 1, MSG_WAIT, 1);
      ret = select_menu("確認", select_no_yes, 0, 25, 8);
      if(ret == 1)
      {
        msg_win("ファイル取得", 0, MSG_WAIT, 0);
        // 新ファイル取得
        ret = web_get_file(new_path, url[opt_2]);
        if(ret < 0)
          msg_win("ファイル取得エラー", 1, MSG_WAIT, 0);
        else
        {
          msg_win("ファイル更新", 0, MSG_WAIT, 0);
          // エラーが無ければ、旧ファイル削除、新ファイルリネーム
          ret = sceIoRemove(old_path);
          if(ret < 0)
            msg_win("旧ファイル削除エラー", 1, MSG_WAIT, 0);
          ret = sceIoRename(new_path, old_path);
          if(ret < 0)
            msg_win("リネームエラー", 1, MSG_WAIT, 0);
          else
            mod = 1;
        }
      }
    }
    // 切断
    msg_win("切断", 0, MSG_WAIT, 0);
    net_disconnect();

    // 状況に応じて再起動/UMD_ID再読込
    if(mod == 1)
    {
      switch(opt_2)
      {
        case 0:
          msg_win("データ再読込", 0, MSG_WAIT, 0);
          get_umd_name(NULL, NULL, NULL, 1);
          text[0] = "更新完了";
          text[1] = "◎を押してください";
          text[2] = "\0";
          dialog(text);
          break;

        case 1:
          text[0] = "更新完了";
          text[1] = "◎を押してください。再起動します";
          text[2] = "\0";
          dialog(text);
          reboot(0);
          break;
      }
    }
    else
    {
      text[0] = "◎を押してください";
      text[1] = "\0";
      dialog(text);
    }
  }
  return 0;
}

/*---------------------------------------------------------------------------
---------------------------------------------------------------------------*/
int soft_reboot(char *dir, char *file, file_type type, int opt_1, int opt_2)
{
  char *msg[2] = { "再起動しますか？", "終了しますか？" };
  int ret;

  msg_win("", 0, MSG_CLEAR, 0);
  msg_win(msg[opt_1], 1, MSG_WAIT, 1);
  ret = select_menu("確認", select_no_yes, 0, 25, 8);
  if(ret == -1)
    return CANCEL;

  if(ret == 1)
    reboot(opt_1);

  return DONE;
}

int remove_file(char *dir, char *file, file_type type, int opt_1, int opt_2)
{
  char path[MAX_PATH];
  int ret;

  strcpy(path, dir);
  strcat(path, file);

  msg_win("", 0, MSG_CLEAR, 0);
  msg_win("削除しますか？", 0, MSG_WAIT, 0);
  ret = select_menu("削除", select_no_yes, 0, 28, 10);
  if(ret == CANCEL)
    return CANCEL;
  if(ret == YES)
    ret = sceIoRemove(path);
  return DONE;
}

int fix_header(char *dir, char *file, file_type type, int opt_1, int opt_2)
{
  char path[MAX_PATH];
  int ret;
  char header_size[1];

  header_size[0] = 0x00;

  strcpy(path, dir);
  strcat(path, file);

  msg_win("", 0, MSG_CLEAR, 0);
  msg_win("ヘッダー調整しますか？", 0, MSG_WAIT, 0);
  ret = select_menu("調整", select_no_yes, 0, 28, 10);
  if(ret == CANCEL)
    return CANCEL;
  if(ret == YES)
    ret = ms_write(header_size, path, 0x04, 1);
  return DONE;
}

int boot_iso(char *dir, char *file, file_type type, int opt_1, int opt_2)
{
  char path[MAX_PATH];
  SEConfig config;
  int ret;

  strcpy(path, dir);
  strcat(path, file);

  sctrlSEUmountUmd();
  sctrlSESetDiscOut(1);
  sctrlSEGetConfig(&config);
  switch(config.umdmode)
  {
    // Normal
    case 0:
      sctrlSEMountUmdFromFile(path, 0, config.useisofsonumdinserted);
      break;
    // OE isofs
    case 1:
      sctrlSEMountUmdFromFile(path, 1, 1);
      break;
    // M33 Driver
    case 2:
      sctrlSEMountUmdFromFile(path, 1, 1);
      break;
    // Sony NP9660
    case 3:
      sctrlSEMountUmdFromFile(path, 1, 1);
      break;
  }

//  sctrlSESetUmdFile(path);

//  sceKernelDelayThread(5000000);
//  sceUmdActivate(1, "disc0:");
//  sceKernelDelayThread(5000000);
//  sceUmdWaitDriveStat(PSP_UMD_READY);
//  ModuleMgrForKernel_1B91F6EC("disc0:/PSP_GAME/SYSDIR/EBOOT.BIN", 0, NULL);
//  sctrlKernelLoadExecVSHDisc("disc0:/PSP_GAME/SYSDIR/EBOOT.BIN", NULL);
//  sctrlKernelLoadExecVSHDisc("disc0:/PSP_GAME/SYSDIR/EBOOT.BIN", NULL);
//  sctrlKernelLoadExecVSHWithApitype();
  ret = sceKernelLoadModule("loadprx.prx", 0, NULL);
  ret = sceKernelStartModule(ret, strlen(path)+1, path, NULL, NULL);
  return DONE;
}
