/*
 * eboot.h
 *
 *  Created on: 2009/12/29
 *      Author: takka
 */

#ifndef EBOOT_H_
#define EBOOT_H_

int eboot_read(void *buf, int max_buf, const char* path, file_type type);
int eboot_write( void *buf, int size, const char* path, file_type type);

#endif /* EBOOT_H_ */
