/*
 * ciso.c
 *
 *  Created on: 2009/10/12
 *      Author: takka
 */

#include <zlib.h>
#include <stdio.h>
#include <string.h>

typedef struct ciso_header
{
    unsigned char magic[4];         /* +00 : 'C','I','S','O'                 */
    unsigned long header_size;      /* +04 : header size (==0x18)            */
    unsigned long long total_bytes; /* +08 : number of original data size    */
    unsigned long block_size;       /* +10 : number of compressed block size */
    unsigned char ver;              /* +14 : version 01                      */
    unsigned char align;            /* +15 : align of index value            */
    unsigned char rsv_06[2];        /* +16 : reserved                        */
}CISO_H;

#define CISO_HEADER_SIZE (0x18)

/*
note:

file_pos_sector[n]  = (index[n]&0x7fffffff) << CISO_H.align
file_size_sector[n] = ( (index[n+1]&0x7fffffff) << CISO_H.align) - file_pos_sector[n]

if(index[n]&0x80000000)
  // read 0x800 without compress
else
  // read file_size_sector[n] bytes and decompress data
*/

/*
00 01 02 03 04 05 06 07   08 09 0A 0B 0C 0D 0E 0F   10 11 12 13 14 15 16 17
43 49 53 4F 00 00 00 00 - 00 78 C4 00 00 00 00 00 - 00 08 00 00 01 00 00 00
C  I  S  O  ???????????   0x0000 00C47800           0x00000800  01 00 00 00

*/

z_stream z;

int inflate_cso(char* o_buff, int o_size, char* i_buff, int i_size);
int cso_read(char *o_buff, int sector, int o_size, char *path);
int deflate_cso(char* o_buff, int o_size, char* i_buff, int i_size, int level);
int cso_write(char *i_buff, int sector, int i_size, int level, char *path);

// deflateの解凍を行う
// char* o_buff 解凍先
// int o_size   解凍先バッファサイズ
// char* i_buff 入力
// int i_size   入力サイズ
// 返値 解凍後のサイズ / エラーの場合は負を返す
int inflate_cso(char* o_buff, int o_size, char* i_buff, int i_size)
{
    int status;
    int size;

    // 初期化
    z.zalloc = Z_NULL;
    z.zfree = Z_NULL;
    z.opaque = Z_NULL;
    z.next_in = Z_NULL;
    z.avail_in = 0;
    if(inflateInit2(&z, -15) != Z_OK)
    {
      // エラー処理
      return -1;
    }

    z.next_in = (unsigned char*)i_buff;
    z.avail_in = i_size;
    z.next_out = (unsigned char*)o_buff;
    z.avail_out = o_size;

    status = inflate(&z, Z_FINISH);
    // エラー処理

    // 出力サイズ
    size = o_size - z.avail_out;

    if (inflateEnd(&z) != Z_OK) {
      // エラー処理
      return -1;
    }

    return size;
}

// deflateの圧縮を行う
// char* o_buff 圧縮先
// int o_size   圧縮先バッファサイズ
// char* i_buff 入力
// int i_size   入力サイズ
// int level    圧縮レベル
// 返値 圧縮後のサイズ / エラーの場合は負を返す

int deflate_cso(char* o_buff, int o_size, char* i_buff, int i_size, int level)
{
    int status;
    int size;

    // 初期化
    z.zalloc = Z_NULL;
    z.zfree = Z_NULL;
    z.opaque = Z_NULL;
    z.next_in = Z_NULL;
    z.avail_in = 0;

    if(deflateInit2(&z, level , Z_DEFLATED, -15, 8, Z_DEFAULT_STRATEGY) != Z_OK)
    {
      // エラー処理
      return -1;
    }

    z.next_in = (unsigned char*)i_buff;  /* 入力ポインタを元に戻す */
    z.avail_in = i_size; /* データを読む */
    z.next_out = (unsigned char*)o_buff;        /* 出力ポインタ */
    z.avail_out = o_size;    /* 出力バッファ残量 */

    status = deflate(&z, Z_FINISH); /* すべて圧縮 */
    // エラー処理

    size = o_size - z.avail_out;

    if (deflateEnd(&z) != Z_OK) {
      // エラー処理
      return -2;
    }

    return size;
}


// ファイル名、セクタ番号、読取り長さを指定して、CSOから読込む
// 返値 実際に読み込んだ長さ / エラーの場合は-1を返す
int cso_read(char *o_buff, int sector, int o_size, char *path)
{
  FILE *fp;
  CISO_H header;
  int sector_num;
  unsigned long long int pos = 0;
  unsigned long long int next_pos = 0;
  int i_size;
  unsigned int flag;
  char i_buff[0x0800 * 2];
  int size;

  fp = fopen(path, "rb");
  // オープンエラー処理

  // ヘッダー読込
  fread(&header, CISO_HEADER_SIZE, 1, fp);

  // 読込セクタ数を計算
  sector_num = (o_size + header.block_size - 1) / header.block_size;

  // セクタ番号から実際に読み込む場所を確認
  // 1セクタずつ読込、最終セクタでは必要なバイト数だけを転送

  while(sector_num > 0)
  {
    // セクタ番号からファイル位置と長さを取得
    fseek(fp, CISO_HEADER_SIZE + (sector * 4), SEEK_SET);
    fread(&pos, 4, 1, fp);
    flag = pos & 0x80000000;
    pos = (pos & 0x7fffffff) << header.align;
    fread(&next_pos, 4, 1, fp);
    i_size = ((next_pos & 0x7fffffff) << header.align) - pos;

    // １セクタを読込
    fseek(fp, pos, SEEK_SET);

    if(flag != 0)
    {
      size = fread(o_buff, header.block_size, 1, fp);
    }
    else
    {
      // 圧縮済みならば

      fread(&i_buff, i_size, 1, fp);

      // 指定バッファに展開
      size = inflate_cso(o_buff, header.block_size, i_buff, i_size);
      // エラー処理
    }

    o_buff += header.block_size;
    sector++;
    sector_num--;

  }

  fclose(fp);
  return 0;
}


/*
セクタ番号、書込み長さを指定して、CSOに書込み
返値 実際に書き込んだ長さ / エラーの場合は-1を返す
*/
int cso_write(char *i_buff, int sector, int i_size, int level, char *path)
{
  FILE* fp;
  CISO_H header;
  int sector_num;
  unsigned long long int pos = 0;
  unsigned long long int next_pos = 0;
  int o_size;
  unsigned int flag;
  char o_buff[0x0800 * 2];
  int size;

  fp = fopen(path, "r+b");
  // オープンエラー処理

  // ヘッダー読込
  fread(&header, CISO_HEADER_SIZE, 1, fp);

  // 書込セクタ数を計算
  sector_num = (i_size + header.block_size - 1) / header.block_size;

  // セクタ番号から実際に読み込む場所を確認
  // 1セクタずつ読込、最終セクタでは必要なバイト数だけを転送

  while(sector_num > 0)
  {
    // セクタ番号からファイル位置と長さを取得
    fseek(fp, CISO_HEADER_SIZE + (sector * 4), SEEK_SET);
    fread(&pos, 4, 1, fp);
    flag = pos & 0x80000000;
    pos = (pos & 0x7fffffff) << header.align;
    fread(&next_pos, 4, 1, fp);
    o_size = ((next_pos & 0x7fffffff) << header.align) - pos;


    // １セクタを書込み
    fseek(fp, pos, SEEK_SET);

    if(flag != 0)
    {
      size = fwrite(i_buff, header.block_size, 1, fp);
    }
    else
    {
      // 圧縮済みならば
      // 指定バッファに圧縮
      size = deflate_cso(o_buff, header.block_size, i_buff, header.block_size, 9);
      // エラー処理

      // 圧縮エラー
      if(size < 0)
      {
        // エラー処理
      }
      else
        if(o_size > size)
        {
          memset(&o_buff[size], 0, o_size - size);
        }
        else
          // サイズオーバー
          if(o_size < size)
          {
            // エラー処理
            fclose(fp);
            return -1;
          }
      fwrite(&o_buff, o_size, 1, fp);
    }

    i_buff += header.block_size;
    sector++;
    sector_num--;
  }

  fclose(fp);
  return 0;
}
