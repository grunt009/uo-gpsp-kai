/*
 * error_code.h
 *
 *  Created on: 2009/10/17
 *      Author: takka
 */

#define OPEN_ERROR      -1
#define CLOSE_ERROR     -2
#define READ_ERROR      -3
#define WRITE_ERROR     -4
#define DECRYPT_ERROR   -5
#define DEFLATE_ERROR   -6
#define INFLATE_ERROR   -7
#define INIT_ERROR      -8
#define NOT_CRYPT_FILE  -9
#define NO_FILE         -10

