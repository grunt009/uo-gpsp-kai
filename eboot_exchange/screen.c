/*
 * screen.c
 *
 *  Created on: 2009/10/17
 *      Author: takka
 */

#include "fbm_print.h"
#include "shnm8x16r.c"
#include "shnmk16.c"

// 480x272 8x16 : 60x17

#define MAX_WIDTH  (60)
#define MAX_HEIGHT (17)

int scr_buff[18][61];

int init_screen()
{
  int loop = 0;
  memset(scr_buff, ' ', sizeof(scr_buff));
  while(loop > MAX_HEIGHT)
    scr_buff[loop++][MAX_WIDTH] = '\0';
}

