#ifndef __CISO_H__
#define __CISO_H__

extern int inflate_cso(char* o_buff, int o_size, char* i_buff, int i_size);
extern int cso_read(char *o_buff, int sector, int o_size, char *path);
extern int deflate_cso(char* o_buff, int o_size, char* i_buff, int i_size, int level);
int cso_write(char *i_buff, int sector, int i_size, int level, char *path);

#endif
