/*
 * umd.h
 *
 *  Created on: 2009/10/23
 *      Author: takka
 */

#ifndef UMD_H_
#define UMD_H_

#define UMD_SEC_SIZE (0x800)

int get_umd_id(char *name);

#endif /* UMD_H_ */
