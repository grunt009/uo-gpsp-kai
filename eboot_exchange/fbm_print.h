/****************************************************************************

  FBM fonts print string headder
                                                                    by mok
                                                                    by takka
****************************************************************************/
#ifndef __FBM_PRINT__
#define __FBM_PRINT__

#include <psptypes.h>

// 構造体の型宣言とfbm_colmix系の関数はローカルでも良いかも

typedef struct
{
	u16 fontcnt;
	u16 mapcnt;
	u16 defaultchar;
	u8  width;
	u8  height;
	u8  byteperchar;
} __attribute__ ((packed)) fbm_control_t;

typedef struct
{
	u16 start;
	u16 end;
	u16 distance;
} __attribute__ ((packed)) fbm_map_t;

typedef struct
{
	u8  *width;
	u8  *font;
} __attribute__ ((packed)) fbm_font_t;

#define FBM_FONT_FILL	(0x01) // フォント部分をVRAMに書込む
#define FBM_BACK_FILL	(0x10) // BG部分をVRAMに書込む

/*---------------------------------------------------------------------------
  フォント初期化
    m_font: メインフォント (ex.ASCII or UTF-8N)
    s_font: サブフォント (ex.UTF-8N)
    m_base: 将来用
    s_base: 将来用
    フォントデータへのアドレスを指定
    文字描画時にはs_font -> m_fontの順でデータを検索する
    s_fontにNULLを指定したときはm_fontのデータだけで描画する
---------------------------------------------------------------------------*/
void fbm_init(u8* m_font, s16 m_base, u8* s_font, s16 s_base);

/*---------------------------------------------------------------------------
  フォントの解放（メモリの確保はしていないので、実行しなくても実害はなし）
---------------------------------------------------------------------------*/
void fbm_freeall();

/*---------------------------------------------------------------------------
  描画文字列の長さを取得
    str: 文字列(UTF-8N)
    mx : 横描画倍率
    返値: 描画ドット数
---------------------------------------------------------------------------*/
u16 fbm_getwidth(s8 *str, u16 mx);

/*---------------------------------------------------------------------------
  XYを文字数指定で文字列表示(昔のBASICのLOCATE+PRINT)
    col: 横位置 (0-fbmMaxCol) ※倍率には左右されないので注意
    row: 縦位置 (0-fbmMaxRow) ※倍率には左右されないので注意
    str: 文字列(UTF-8N)
    color: 文字色
    back: 背景色
    fill: 書込みモード (ex.FBM_FONT_FILL | FBM_BACK_FILL),
    rate: 混合比 (0-100 or -1--101) ※負の場合はVRAMの色を反転し合成（減色）
    mx : 横描画倍率
    my : 縦描画倍率
    返値: 成功 0 / 失敗 -1
---------------------------------------------------------------------------*/
u16 fbm_printCR(u16 col, u16 row, s8 *str, u32 color, u32 back, u8 fill, s16 rate, u16 mx, u16 my);

/*---------------------------------------------------------------------------
  XYをドット指定で文字列表示
    x: 横位置 (0-479)
    y: 縦位置 (0-271)
    str: 文字列(UTF-8N)
    color: 文字色
    back: 背景色
    fill: 書込みモード (ex.FBM_FONT_FILL | FBM_BACK_FILL),
    rate: 混合比 (0-100 or -1--101) ※負の場合はVRAMの色を反転し合成（減色）
    mx : 横描画倍率
    my : 縦描画倍率
    返値: 成功 0 / 失敗 -1
---------------------------------------------------------------------------*/
u16 fbm_printXY(u16 x, u16 y, s8 *str, u32 color, u32 back, u8 fill, s16 rate, u16 mx, u16 my);

/*---------------------------------------------------------------------------
  ベースVRAMアドレス + XYをドット指定で文字列表示
    vram: ベースVRAMアドレス ※VRAM以外でもOK
    bufferwidth: バッファの1ラインあたりのドット数
    pixelformat: カラーフォーマット (0=16bit, 1=15bit, 2=12bit, 3=32bit)
    x: 横位置 (0-479)
    y: 縦位置 (0-271)
    str: 文字列(UTF-8N)
    color: 文字色
    back: 背景色
    fill: 書込みモード (ex.FBM_FONT_FILL | FBM_BACK_FILL),
    rate: 混合比 (0-100 or -1--101) ※負の場合はVRAMの色を反転し合成（減色）
    mx : 横描画倍率
    my : 縦描画倍率
    返値: 成功 0 / 失敗 -1
---------------------------------------------------------------------------*/
u16 fbm_printVRAM(void *vram, u16 bufferwidth, u16 pixelformat, u16 x, u16 y, s8 *str, u32 color, u32 back, u8 fill, s16 rate, u16 mx, u16 my);

/*---------------------------------------------------------------------------
  VRAM文字描画サブルーチン
    vram: 描画開始VRAMアドレス ※VRAM以外でもOK
    bufferwidth: バッファの1ラインあたりのドット数
    index: フォント番号
    fonttype:  (0=メイン, 1=サブ)
    height: フォントの高さ
    byteperline: フォント1ラインで使用しているバイト数
    color: 文字色
    back: 背景色
    fill: 書込みモード (ex.FBM_FONT_FILL | FBM_BACK_FILL),
    rate: 混合比 (0-100 or -1--101) ※負の場合はVRAMの色を反転し合成（減色）
    mx : 横描画倍率
    my : 縦描画倍率
---------------------------------------------------------------------------*/
void fbm_printSUB16(void *vram, u16 bufferwidth, u16 index, u16 fonttype, u16 height, u16 byteperline, u32 color, u32 back, u8 fill, s16 rate, u16 mx, u16 my);
void fbm_printSUB32(void *vram, u16 bufferwidth, u16 index, u16 fonttype, u16 height, u16 byteperline, u32 color, u32 back, u8 fill, s16 rate, u16 mx, u16 my);

/*---------------------------------------------------------------------------
  VRAM カラー合成
    vr: VRAMアドレス
    color: 合成色
    rate: 混合比 (0-100)
---------------------------------------------------------------------------*/
u32 fbm_colmix_565(void *vr, u32 color, s16 rate);
u32 fbm_colmix_5551(void *vr, u32 color, s16 rate);
u32 fbm_colmix_4444(void *vr, u32 color, s16 rate);
u32 fbm_colmix_8888(void *vr, u32 color, s16 rate);
u32 fbm_colmixrev_565(void *vr, u32 color, s16 rate);
u32 fbm_colmixrev_5551(void *vr, u32 color, s16 rate);
u32 fbm_colmixrev_4444(void *vr, u32 color, s16 rate);
u32 fbm_colmixrev_8888(void *vr, u32 color, s16 rate);

#endif
