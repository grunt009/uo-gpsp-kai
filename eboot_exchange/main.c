/*
	eboot_exchange ver 2.7
 */

#include <pspkernel.h>
#include <kubridge.h>
#include <pspdebug.h>
#include <pspsdk.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <systemctrl_se.h>
#include <psppower.h>

#include "pspdecrypt.h"
#include "ciso.h"
#include "umd.h"
#include "key.h"

#include "fbm_print.h"
#include "shnm8x16r.c"
#include "shnmk16.c"



PSP_MODULE_INFO( "eboot_exchange", 0, 1, 1 );
PSP_MAIN_THREAD_ATTR(THREAD_ATTR_USER | THREAD_ATTR_VFPU);

#define MID_STR(x, str, num) (x - (fbm_getwidth(str, num) / 2))

#define CLEAR_LINE "                                    "
#define MENU_TITLE " eboot exchange v2.8 "
#define MENU_MSG_1 " U/D/R/L ... SELECT ISO / ○ ... RIP / × ... EXIT "
#define MENU_MSG_2 " △ ... EXCHANGE / □ ... REPAIR "
#define MENU_MSG_3 " SELECT + △/□ ... TARGET ALL ISO FILE "

#define MSG_LINE 150

#define READ_EBOOT_BIN        " READ EBOOT.BIN "
#define NOT_CRYPT_EBOOT_BIN   " NOT CRYPT EBOOT.BIN "
#define DECRYPT_ERROR         " DECRYPT EBOOT.BIN ERROR "
#define WRITE_EBOOT_BIN       " WRITE EBOOT.BIN "
#define EXCHANGE_DONE         " EXCHANGE DONE "
//
#define READ_BACKUP         " READ BACKUP FILE "
#define WRITE_BACKUP        " WRITE BACKUP FILE "
#define REPAIR_EBOOT_BIN    " REPAIR EBOOT.BIN "
#define REPAIR_DONE         " REPAIR DONE "
#define NO_BACKUP           " NO BACKUP "
#define NOT_BACKUP_FILE     " NOT BACKUP FILE "

#define ALL_ECHANGE         " ALL EXCHANGE "
#define ALL_REPAIR          " ALL REPAIR "
#define ALL_ECHANGE_DONE    " ALL EXCHANGE DONE "
#define ALL_REPAIR_DONE     " ALL REPAIR DONE "

#define EBOOT_BIN_SIZE_OVER " EBOOT.BIN SIZE OVER "

#define MENU_XY(x, y, str, num) fbm_printXY(MID_STR(x, str, num), y, str, 0xFFFFFFFF, 0, FBM_FONT_FILL | FBM_BACK_FILL, 100, num, num);

#define ISO_PATH        "ms0:/iso/"
#define EBOOT_NAME      "EBOOT.BIN"
#define EBOOT_NAME_LEN  9

int exit_callback(int arg1, int arg2, void *common);
int CallbackThread(SceSize args, void *argp);
int SetupCallbacks(void);
int LoadStartModule(char *module, int partition);

int main(void);

int read_dir(char *dir_name);
void set_clock(int cpu, int bus);
void wait_button_up(void);
void wait_button_down(void);
void print_menu(char *iso_name, int backup_mode, int patch_mode);

int get_eboot(char* path, int* pos, int* size, int* size_pos, int* mode);
void eboot_exchange(char* path, int backup_mode, int patch_mode);
void eboot_repair(char* path);

void chg_file_mode(char* path);

int umd(void);

#define EBOOT_MAX_SIZE (10*1024*1024) // 8*512

static char i_buff[EBOOT_MAX_SIZE] __attribute__((aligned(0x40)));
static char o_buff[EBOOT_MAX_SIZE] __attribute__((aligned(0x40)));

// HOMEキー用の初期設定
int exit_callback(int arg1, int arg2, void *common)
{
    sceKernelExitGame();
    return 0;
}

int CallbackThread(SceSize args, void *argp)
{
    int cbid;

    cbid = sceKernelCreateCallback("Exit Callback", exit_callback, NULL);
    sceKernelRegisterExitCallback(cbid);

    sceKernelSleepThreadCB();

    return 0;
}

int SetupCallbacks(void)
{
    int thid = 0;

    thid = sceKernelCreateThread("update_thread", CallbackThread, 0x11, 0xFA0, 0, 0);
    if (thid >= 0) {
        sceKernelStartThread(thid, 0, 0);
    }

    return thid;
}

int LoadStartModule(char *module, int partition)
{
    SceUID mod = kuKernelLoadModule(module, 0, NULL);

    if (mod < 0)
        return mod;

    return sceKernelStartModule(mod, 0, NULL, NULL, NULL);
}

char iso_name[256][256];
int dir_flag[256];

int read_dir(char *dir_name)
{
  DIR *dp;
  struct dirent *entry;
  int num;
  int file_num = 0;

  // ディレクトリデータの取得
  dp = opendir(dir_name);

  // ISOフォルダがない場合
  if(dp == NULL)
    return -1;

  // ファイルデータの取得
  while((entry = readdir(dp)) != NULL){
    num = strlen(entry->d_name);

    switch(entry->d_stat.st_mode & FIO_S_IFMT)
    {

      // ISOファイルの場合
      case FIO_S_IFREG:
        if(strncasecmp(&entry->d_name[num - 4], ".iso", 4) == 0)
        {
          strcpy(&iso_name[file_num][0], entry->d_name);
          dir_flag[file_num] = 0;
          file_num++;
        }
        if(strncasecmp(&entry->d_name[num - 4], ".cso", 4) == 0)
        {
          strcpy(&iso_name[file_num][0], entry->d_name);
          dir_flag[file_num] = 0;
          file_num++;
        }

        break;

      // ディレクトリの場合
      case FIO_S_IFDIR:
        if((strcmp(&entry->d_name[0], ".") != 0) && (strcmp(&entry->d_name[0], "..") != 0))
        {
          strcpy(&iso_name[file_num][0], entry->d_name);
          dir_flag[file_num] = 1;
          file_num++;
        }
        break;

    }
  }

  closedir(dp);

  return file_num;
}

// メイン
int main(void)
{
  char now_path[1024] = ISO_PATH;
  char path[1024];
  int dir_level = 1;

  int exit_flag = 0;

  int file_num = 0;
  int select_num = 0;

  int loop;

  int backup_mode;
  int patch_mode;
  int mode;

  static SceCtrlData  data;

  // HOMEボタンを使用可能にする
  SetupCallbacks();

  SceUID mod = LoadStartModule("pspdecrypt.prx", PSP_MEMORY_PARTITION_KERNEL);
  if (mod < 0)
  {
    MENU_XY(240, 110, (s8*)" NOT LOAD PSPDECRYPT.PRX ", 1);
    MENU_XY(240, MSG_LINE, (s8*)" PUSH ANY BUTTON ", 1);

    wait_button_down();
    wait_button_up();

    sceKernelExitGame();
  }

  backup_mode = 0;
  patch_mode = 0;
  mode = 0;

  fbm_init(shnm8x16r, 0, shnmk16, 0);

  pspDebugScreenInit();
  print_menu("", backup_mode, patch_mode);

//  cso_read(o_buff, 0x100, 1024 * 1024, "ms0:/ISO/igo.cso");
//
//  FILE* fp;
//  fp = fopen("ms0:/PSP/GAME/ebt_exchange/igo.tst", "wb");
//  fwrite(o_buff, 1024*1024, 1, fp);
//  fclose(fp);
//
//  cso_write(o_buff, 0x100, 1024 * 1024, 9, "ms0:/PSP/GAME/ebt_exchange/igo.cso");
//
//  cso_read(o_buff, 0x100, 1024 * 1024, "ms0:/PSP/GAME/ebt_exchange/igo.cso");
//
//  fp = fopen("ms0:/PSP/GAME/ebt_exchange/igo_ext.tst", "wb");
//  fwrite(o_buff, 1024*1024, 1, fp);
//  fclose(fp);
//
//
//  sceKernelExitGame();

  // ディレクトリデータの取得
  file_num = read_dir(now_path);

  // ISOフォルダがない場合
  if(file_num < 1)
  {
    MENU_XY(240, 110, (s8*)" NO ISO FOLDER or ISO/CSO DATA ", 1);
    MENU_XY(240, MSG_LINE, (s8*)" PUSH ANY BUTTON ", 1);

    wait_button_down();
    wait_button_up();

    sceKernelExitGame();
  }

  print_menu(&iso_name[0][0], backup_mode, patch_mode);
  MENU_XY(240, MSG_LINE, (s8*)now_path, 1);

  // ボタンから手が離れるまで待つ
  wait_button_up();

  // メニューのメインループ
  while(exit_flag == 0)
  {
    get_button_wait(&data);
    switch(data.Buttons)
    {
      case PSP_CTRL_START:
        wait_button_up();
        mode = (mode + 1) % 4;
        patch_mode = mode & 0x01;
        backup_mode = (mode >> 1) & 0x01;
        print_menu(&iso_name[select_num][0], backup_mode, patch_mode);
        MENU_XY(240, MSG_LINE, (s8*)CLEAR_LINE, 1);
        MENU_XY(240, MSG_LINE, (s8*)now_path, 1);
        break;

      case PSP_CTRL_TRIANGLE:
        if(dir_flag[select_num] == 0)
        {
          wait_button_up();
          set_clock(333,166);
          sprintf(path, "%s/%s", now_path, &iso_name[select_num][0]);
          eboot_exchange(path, backup_mode, patch_mode);
          set_clock(222,111);
        }
        break;

      case (PSP_CTRL_TRIANGLE | PSP_CTRL_SELECT):
        set_clock(333,166);
        MENU_XY(240, 90, (s8*)CLEAR_LINE, 1);
        MENU_XY(240, 90, (s8*)ALL_ECHANGE, 1);
        for(loop = 0; loop < file_num; loop++)
        {
          if(dir_flag[loop] == 0)
          {
            MENU_XY(240, 110, (s8*)CLEAR_LINE, 1);
            MENU_XY(240, 110, (s8*)&iso_name[loop][0], 1);
            sprintf(path, "%s/%s", now_path, &iso_name[loop][0]);
            eboot_exchange(path, backup_mode, patch_mode);
          }
        }
        print_menu(&iso_name[select_num][0], backup_mode, patch_mode);
        MENU_XY(240, 90, (s8*)CLEAR_LINE, 1);
        MENU_XY(240, MSG_LINE, (s8*)CLEAR_LINE, 1);
        MENU_XY(240, MSG_LINE, (s8*)ALL_ECHANGE_DONE, 1);
        set_clock(222,111);
        wait_button_up();
        break;

      case PSP_CTRL_SQUARE:
        if(dir_flag[select_num] == 0)
        {
          wait_button_up();
          set_clock(333,166);
          sprintf(path, "%s/%s", now_path, &iso_name[select_num][0]);
          eboot_repair(path);
          set_clock(222,111);
        }
        break;

      case (PSP_CTRL_SQUARE | PSP_CTRL_SELECT):
        set_clock(333,166);
        MENU_XY(240, 90, (s8*)CLEAR_LINE, 1);
        MENU_XY(240, 90, (s8*)ALL_REPAIR, 1);
        for(loop = 0; loop < file_num; loop++)
        {
          if(dir_flag[loop] == 0)
          {
            MENU_XY(240, 110, (s8*)CLEAR_LINE, 1);
            MENU_XY(240, 110, (s8*)&iso_name[loop][0], 1);
            sprintf(path, "%s/%s", now_path, &iso_name[loop][0]);
            eboot_repair(path);
          }
        }
        print_menu(&iso_name[select_num][0], backup_mode, patch_mode);
        MENU_XY(240, 90, (s8*)CLEAR_LINE, 1);
        MENU_XY(240, MSG_LINE, (s8*)CLEAR_LINE, 1);
        MENU_XY(240, MSG_LINE, (s8*)ALL_REPAIR_DONE, 1);
        set_clock(222,111);
        wait_button_up();
        break;

      case PSP_CTRL_CIRCLE:
        wait_button_up();
        set_clock(333,166);
        umd();
        set_clock(222,111);
        break;

      case PSP_CTRL_CROSS:
        exit_flag = 1;
        wait_button_up();
        break;

      case PSP_CTRL_LTRIGGER:
      case PSP_CTRL_LEFT:
        wait_button_up();
        select_num--;
        if(select_num < 0) select_num = file_num - 1;
        print_menu(&iso_name[select_num][0], backup_mode, patch_mode);
        MENU_XY(240, MSG_LINE, (s8*)CLEAR_LINE, 1);
        MENU_XY(240, MSG_LINE, (s8*)now_path, 1);
        break;

      case PSP_CTRL_RTRIGGER:
      case PSP_CTRL_RIGHT:
        wait_button_up();
        select_num++;
        if(select_num >= file_num) select_num = 0;
        print_menu(&iso_name[select_num][0], backup_mode, patch_mode);
        MENU_XY(240, MSG_LINE, (s8*)CLEAR_LINE, 1);
        MENU_XY(240, MSG_LINE, (s8*)now_path, 1);
        break;

      case PSP_CTRL_UP:
        if(dir_level != 0)
        {
          wait_button_up();
          strcpy(path, now_path);

          loop = strlen(path) - 2;
          while(path[loop] != '/')
            loop--;
          path[loop + 1] = '\0';

          file_num = read_dir(path);
          if(file_num < 1)
          {
            file_num = read_dir(now_path);
          }
          else
          {
            select_num = 0;
            dir_level--;
            strcpy(now_path, path);
          }

          print_menu(&iso_name[select_num][0], backup_mode, patch_mode);
          MENU_XY(240, MSG_LINE, (s8*)CLEAR_LINE, 1);
          MENU_XY(240, MSG_LINE, (s8*)now_path, 1);
        }
        break;

      case PSP_CTRL_DOWN:
        if(dir_flag[select_num] == 1)
        {
          wait_button_up();
          sprintf(path, "%s%s/", now_path, &iso_name[select_num][0]);
          file_num = read_dir(path);
          if(file_num < 1)
          {
            file_num = read_dir(now_path);
          }
          else
          {
            dir_level++;
            select_num = 0;
            strcpy(now_path, path);
          }
          print_menu(&iso_name[select_num][0], backup_mode, patch_mode);
          MENU_XY(240, MSG_LINE, (s8*)CLEAR_LINE, 1);
          MENU_XY(240, MSG_LINE, (s8*)now_path, 1);
        }
        break;

    }
  }

  fbm_freeall();
  sceKernelExitGame();
  return 0;
}

void set_clock(int cpu, int bus)
{
  scePowerSetClockFrequency(cpu, cpu, bus);
}


void print_menu(char *iso_name, int backup_mode, int patch_mode)
{
  MENU_XY(240, 10, (s8*)MENU_TITLE, 1);
  MENU_XY(240, 50, (s8*)CLEAR_LINE, 1);
  if(backup_mode == 0)
  {
    MENU_XY(240, 50, (s8*)" NO BACKUP MODE ", 1);
  }
  else
  {
    MENU_XY(240, 50, (s8*)" BACKUP MODE ", 1);
  }

  MENU_XY(240, 70, (s8*)CLEAR_LINE, 1);
  if(patch_mode == 0)
  {
    MENU_XY(240, 70, (s8*)" NO PATCH MODE ", 1);
  }
  else
  {
    MENU_XY(240, 70, (s8*)" PATCH MODE ", 1);
  }

  MENU_XY(240, 110, (s8*)CLEAR_LINE, 1);
  MENU_XY(240, 110, (s8*)iso_name, 1);

  MENU_XY(240, 200, (s8*)MENU_MSG_1, 1);
  MENU_XY(240, 220, (s8*)MENU_MSG_2, 1);
  MENU_XY(240, 240, (s8*)MENU_MSG_3, 1);
  sceKernelDcacheWritebackAll();
}

int get_eboot(char* path, int* pos, int* size, int* size_pos, int* mode)
{
  FILE* fp;
  int ptr;
  int num;

  num = strlen(path);
  // 20～63セクタ(44セクタ分)を読み込む //Ys7のエラーの原因
  if(strncasecmp(&path[num - 4], ".iso", 4) == 0)
  {
    fp = fopen(path, "rb");
    fseek( fp, 20 * 0x800, SEEK_SET);
    fread(o_buff, 0x800, 44, fp);
    fclose(fp);
    *mode = 0;
  }

  if(strncasecmp(&path[num - 4], ".cso", 4) == 0)
  {
    cso_read(o_buff, 20, 44 * 0x800, path);
    *mode = 1;
  }

  ptr = 0;

  do{
    while(o_buff[ptr++] != 'E');
  }while(strncasecmp(&o_buff[ptr], "BOOT.BIN", 8) != 0);

  ptr--;

  // ファイル名 - 0x1f にファイル先頭セクタ
  memcpy(pos, &o_buff[ptr - 0x1f], 4);

  // ファイル名 - 0x17 にファイルサイズ
  memcpy(size, &o_buff[ptr - 0x17], 4);

  // ファイルサイズの位置
  *size_pos = 20 * 0x800 + ptr - 0x17;

  return 0;
}

void eboot_exchange(char* path, int backup_mode, int patch_mode)
{
  int mode;
  int pos;
  int size_pos;
  int i_size;
  int o_size;

  FILE* fp;
  char ebt_path[1024];
  const unsigned char patch_1[] = { 0xE0, 0xFF, 0xBD, 0x27, 0x05, 0x05, 0x02, 0x3C, 0x0C, 0x00, 0xB3, 0xAF};
  const unsigned char patch_2[] = { 0x00, 0x40, 0x05, 0x34, 0x05, 0x05, 0x04, 0x3C, 0x99, 0x81, 0x05, 0x0C};

  // パッチ
  //    addiu  r29,r29,-$20                       ;0000010C[27BDFFE0,'...''] E0 FF BD 27
  // +  lui    r2,$0505                           ;00000110[3C020505,'...<'] 05 05 02 3C
  //    sw     r19,$c(r29)                        ;00000114[AFB3000C,'....'] 0C 00 B3 AF
  // アンデットナイツ
  //47 81 05 0C 00 40 05 34 05 05 04 3C 99 81 05 0C

  int num;

  // ISOからの読込み
  MENU_XY(240, MSG_LINE, (s8*)CLEAR_LINE, 1);
  MENU_XY(240, MSG_LINE, (s8*)READ_EBOOT_BIN, 1);

  // EBOOT.BINの位置/サイズ/サイズデータの位置を取得
  get_eboot(path, &pos, &i_size, &size_pos, &mode);

  if(i_size > EBOOT_MAX_SIZE)
  {
    MENU_XY(240, MSG_LINE, (s8*)CLEAR_LINE, 1);
    MENU_XY(240, MSG_LINE, (s8*)EBOOT_BIN_SIZE_OVER, 1);
    return;
  }

  if(mode == 0)
  {
    fp = fopen(path, "rb");
    fseek( fp, pos * 0x800, SEEK_SET);
    fread(i_buff, i_size, 1, fp);
    fclose(fp);
  }
  else
  {
    cso_read(i_buff, pos, i_size, path);
  }

  if(backup_mode != 0)
  {
    // 複合済みか判定
    if(strncmp(&i_buff[0], "~PSP" , 4) == 0)
    {

      // EBTファイルの書込み
      MENU_XY(240, MSG_LINE, (s8*)CLEAR_LINE, 1);
      MENU_XY(240, MSG_LINE, (s8*)WRITE_BACKUP, 1);

      strcpy(ebt_path, path);
      num = strlen(ebt_path);
      strcpy(&ebt_path[num - 4], ".EBT");
      fp = fopen(ebt_path, "wb");
      fwrite(i_buff, i_size, 1, fp);
      fclose(fp);
    }
    else
    {
      // 複合済み
      MENU_XY(240, MSG_LINE, (s8*)CLEAR_LINE, 1);
      MENU_XY(240, MSG_LINE, (s8*)NOT_CRYPT_EBOOT_BIN, 1);
    }
  }

  if(strncmp(&i_buff[1], "ELF" , 3) == 0)
  {
    // 複合化ずみ
    o_size = i_size;
    memcpy(o_buff, i_buff, o_size);
  }
  else
  {
    // 複合化
    o_size = pspDecryptPRX((u8 *)i_buff, (u8 *)o_buff, i_size);
    if((o_size <= 0) || (strncmp(&o_buff[1], "ELF", 3) != 0))
    {
      MENU_XY(240, MSG_LINE, (s8*)CLEAR_LINE, 1);
      MENU_XY(240, MSG_LINE, (s8*)DECRYPT_ERROR, 1);
      return;
    }
  }

  // パッチ
  //    addiu  r29,r29,-$20                       ;0000010C[27BDFFE0,'...''] E0 FF BD 27
  // +  lui    r2,$0505                           ;00000110[3C020505,'...<'] 05 05 02 3C
  //    sw     r19,$c(r29)                        ;00000114[AFB3000C,'....'] 0C 00 B3 AF

  //47 81 05 0C 00 40 05 34 05 05 04 3C 99 81 05 0C

  if(patch_mode != 0)
  {
    num = 0;
    do{
      if(memcmp(&o_buff[num], patch_1, 12) == 0)
        o_buff[num + 4] = 0;
      if(memcmp(&o_buff[num], patch_2, 12) == 0)
        o_buff[num + 4] = 0;
      num++;
    }while(num < o_size);
  }

  // ISOファイルへの書込み
  MENU_XY(240, MSG_LINE, (s8*)CLEAR_LINE, 1);
  MENU_XY(240, MSG_LINE, (s8*)WRITE_EBOOT_BIN, 1);

  chg_file_mode(path);
  if(mode == 0)
  {
    fp = fopen(path, "r+b");
    fseek( fp, pos * 0x800, SEEK_SET);
    fwrite(o_buff, o_size, 1, fp);
    // ファイルサイズの変更
    fseek( fp, size_pos, SEEK_SET);
    fwrite(&o_size, 4, 1, fp);
    fclose(fp);
  }
  else
  {
    cso_write(o_buff, pos, o_size, 9, path);

    // ファイルサイズの変更
    pos = size_pos / 0x800;
    size_pos %= 0x800;
    cso_read(i_buff, pos, 0x800, path);

    memcpy(&i_buff[size_pos], &o_size, 4);

    cso_write(i_buff, pos, 0x800, 9, path);
}

  MENU_XY(240, MSG_LINE, (s8*)CLEAR_LINE, 1);
  MENU_XY(240, MSG_LINE, (s8*)EXCHANGE_DONE, 1);
}

void eboot_repair(char* path)
{
  int mode;
  int pos;
  int size;
  int size_pos;
  get_eboot(path, &pos, &size, &size_pos, &mode);

  FILE* fp;
  char ebt_path[1024];

  int num;

  // BACKUPの読込み
  MENU_XY(240, MSG_LINE, (s8*)CLEAR_LINE, 1);
  MENU_XY(240, MSG_LINE, (s8*)READ_BACKUP, 1);

  strcpy(ebt_path, path);
  num = strlen(ebt_path);
  strcpy(&ebt_path[num - 4], ".EBT");
  fp = fopen(ebt_path, "rb");
  if(fp == NULL)
  {
    MENU_XY(240, MSG_LINE, (s8*)CLEAR_LINE, 1);
    MENU_XY(240, MSG_LINE, (s8*)NO_BACKUP, 1);
    return;
  }
  fseek( fp, 0, SEEK_END );
  size = ftell( fp );
  fseek( fp, 0, SEEK_SET );
  fread(i_buff, size, 1, fp);
  fclose(fp);

  // BACKUPファイルの判定
  if(strncmp(&i_buff[0], "~PSP" , 4) == 0)
  {

    MENU_XY(240, MSG_LINE, (s8*)CLEAR_LINE, 1);
    MENU_XY(240, MSG_LINE, (s8*)REPAIR_EBOOT_BIN, 1);

    // ISOファイルへの書込み
    if(mode == 0)
    {
      fp = fopen(path, "r+b");
      fseek( fp, pos * 0x800, SEEK_SET);
      fwrite(i_buff, size, 1, fp);
      // ファイルサイズの変更
      fseek( fp, size_pos, SEEK_SET);
      fwrite(&size, 4, 1, fp);
      fclose(fp);
    }
    else
    {
      cso_write(i_buff, pos, size, 9, path);

      // ファイルサイズの変更
      pos = size_pos / 0x800;
      size_pos %= 0x800;
      cso_read(i_buff, pos, 0x800, path);

      memcpy(&i_buff[size_pos], &size, 4);

      cso_write(i_buff, pos, 0x800, 9, path);
    }

    MENU_XY(240, MSG_LINE, (s8*)CLEAR_LINE, 1);
    MENU_XY(240, MSG_LINE, (s8*)REPAIR_DONE, 1);
  }
  else
  {
    MENU_XY(240, MSG_LINE, (s8*)CLEAR_LINE, 1);
    MENU_XY(240, MSG_LINE, (s8*)NOT_BACKUP_FILE, 1);
  }

}

void chg_file_mode(char* path)
{
  SceIoStat stat;

  sceIoGetstat(path, &stat);
  stat.st_mode |= (FIO_S_IWUSR | FIO_S_IWGRP | FIO_S_IWOTH);
  sceIoChstat(path, &stat, (FIO_S_IRWXU | FIO_S_IRWXG | FIO_S_IRWXO));
}

int umd()
{
  SceUID umd;
  SceUID ms0;

  char *buff[2] = { i_buff, o_buff };
  int num;
  char str[1024];
  int size;
  int total;
  SceInt64 res;
  char name[16];
  char path[1024];

  total = 0;
  num = 0;
  SceCtrlData  data;

  get_umd_id(name);
  sprintf(path, "ms0:/iso/%s.ISO", name);

  umd = sceIoOpen("umd:", PSP_O_RDONLY, 0777);
  ms0 = sceIoOpen(path, PSP_O_WRONLY | PSP_O_CREAT | PSP_O_TRUNC, 0777);

  while(size = sceIoRead(umd, buff[num], 4096), size > 0)
  {
    total += size;
    sceIoWaitAsync(ms0, &res);
    sceIoWriteAsync(ms0, buff[num], 0x800 * size);
    sprintf(str, "%s WRITE %dMB", name, total / 512);
    MENU_XY(240, MSG_LINE, (s8*)CLEAR_LINE, 1);
    MENU_XY(240, MSG_LINE, (s8*)str, 1);
    num = (num + 1) % 2;
    get_button(&data);
    if((data.Buttons) == PSP_CTRL_CROSS)
    {
        MENU_XY(240, MSG_LINE, (s8*)CLEAR_LINE, 1);
        MENU_XY(240, MSG_LINE, (s8*)"STOP", 1);
        wait_button_down();
        wait_button_up();
        sceIoWaitAsync(ms0, &res);
        sceIoClose(umd);
        sceIoClose(ms0);
        return -1;
    }
  }
  sceIoWaitAsync(ms0, &res);
  sceIoClose(umd);
  sceIoClose(ms0);

  MENU_XY(240, MSG_LINE, (s8*)CLEAR_LINE, 1);
  MENU_XY(240, MSG_LINE, (s8*)" DONE ", 1);

  return 0;
}


