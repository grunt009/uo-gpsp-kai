/****************************************************************************

  FBM fonts print string source
                                                                    by mok
                                                                    by takka
 ****************************************************************************/

#include <pspdisplay.h>
#include <pspge.h>
#include <string.h>
#include "fbm_print.h"

#define FBM_PSP_WIDTH (480)
#define FBM_PSP_HEIGHT (272)
#define FBM_PSP_PIXEL_FORMAT (3)

#define FBM_SIZE_CONTROL (9)
#define FBM_SIZE_MAP (6)

#define MAX_STR (2048)

static fbm_control_t *fbmControl[2];
static fbm_map_t *fbmFontMap[2];
static u8 *font_buf[2];
static fbm_font_t fbmFont;

static u16 fbmMaxCol;
static u16 fbmMaxRow;
static u16 nextx;
static u16 nexty;

static u8 use_subfont;	// 0=single only, other=single+double

static u16 fbm_whence[2];

static void (*fbmPrintSUB)(void *vram, u16 bufferwidth, u16 index, u16 fonttype, u16 height, u16 byteperline, u32 color, u32 back, u8 fill, s16 rate, u16 mx, u16 my);
static u32 (*fbmColMix)(void *vr, u32 color, s16 rate);

static int fbm_readfbm(u8* font, fbm_control_t **control, fbm_map_t **map, u16 *fbm_whence, u8 **buf);
static void fbm_get_index(u16 utf8, u16 *index, u16 *font_num);
static fbm_font_t * fbm_getfont(u16 index, u8 fonttype);
static const s8* utf8decode(const s8 *utf8, u16 *ucs);
static u8 utf8_ucs2(const s8 *utf8, u16 *ucs);
static u16 ucslen(const u16 *ucs);

void fbm_init(u8* m_font, s16 m_base, u8* s_font, s16 s_base)
{

  fbm_freeall();

  fbm_readfbm(m_font, &fbmControl[0], &fbmFontMap[0], &fbm_whence[0], &font_buf[0]);

  if(s_font != NULL)
  {
    use_subfont = 1;
    fbm_readfbm(s_font, &fbmControl[1], &fbmFontMap[1], &fbm_whence[1], &font_buf[1]);
  }

  fbmMaxCol = FBM_PSP_WIDTH / fbmControl[0]->width;
  fbmMaxRow = FBM_PSP_HEIGHT / fbmControl[0]->height;
}

int fbm_readfbm(u8* font_data, fbm_control_t **control, fbm_map_t **map, u16 *fbm_whence, u8 **buf)
{
  *control = (fbm_control_t*)font_data;
  *map = (fbm_map_t*)(font_data + FBM_SIZE_CONTROL);
  *fbm_whence = (u16)(FBM_SIZE_CONTROL + FBM_SIZE_MAP * (*control)->mapcnt);
  *buf = (u8 *)(font_data + *fbm_whence);

  return 0;
}

fbm_font_t* fbm_getfont(u16 index, u8 fonttype)
{
  fbmFont.width = (u8 *)(font_buf[fonttype] + (1 + fbmControl[fonttype]->byteperchar) * index);
  fbmFont.font = (u8 *)(fbmFont.width + 1);

  return &fbmFont;
}

void fbm_freeall()
{
  fbmFontMap[0] = NULL;
  fbmFontMap[1] = NULL;
  font_buf[0] = NULL;
  font_buf[1] = NULL;

  fbm_whence[0] = 0;
  fbm_whence[1] = 0;

  fbmMaxCol = 0;
  fbmMaxRow = 0;

  nextx = 0;
  nexty = 0;
  use_subfont = 0;
}

u16 fbm_getwidth(s8 *str, u16 mx)
{
  u16 i;
  u16 len;
  u16 index;
  u16 width = 0;
  fbm_font_t *font;
  u16 ucs[MAX_STR];
  u16 font_num;

  utf8_ucs2(str, ucs);
  len = ucslen(ucs);

  for (i = 0; i < len; i++)
  {
    fbm_get_index(ucs[i], &index, &font_num);
    font = fbm_getfont(index, font_num);
    width += *(font->width);
  }

  return width * mx;
}

u16 fbm_printCR(u16 col, u16 row, s8 *str, u32 color, u32 back, u8 fill, s16 rate, u16 mx, u16 my)
{
  return fbm_printXY(col * fbmControl[0]->width, row * fbmControl[0]->height, str, color, back, fill, rate, mx, my);
}

u16 fbm_printXY(u16 x, u16 y, s8 *str, u32 color, u32 back, u8 fill, s16 rate, u16 mx, u16 my)
{
  void *vram;
  int  bufferwidth;
  int  pixelformat;
  int  pwidth;
  int  pheight;
  int  unk;

  sceDisplayGetMode(&unk, &pwidth, &pheight);
  sceDisplayGetFrameBuf(&vram, &bufferwidth, &pixelformat, unk);

  if(vram == NULL)
    vram = (void*) (0x40000000 | (u32) sceGeEdramGetAddr());

  return fbm_printVRAM(vram, bufferwidth, pixelformat, x, y, str, color, back, fill, rate, mx, my);
}

u16 fbm_printVRAM(void *vram, u16 bufferwidth, u16 pixelformat,u16 x, u16 y, s8 *str, u32 color, u32 back, u8 fill, s16 rate, u16 mx, u16 my)
{
  u16 i;
  u16 len;
  u16 index;
  u16 font_num;
  u16 ucs[MAX_STR];

  if (bufferwidth == 0) return -1;

  if (x >= 0) nextx = x % FBM_PSP_WIDTH;
  if (y >= 0) nexty = y % FBM_PSP_HEIGHT;

  switch (pixelformat)
  {
    case PSP_DISPLAY_PIXEL_FORMAT_565:
      fbmPrintSUB = fbm_printSUB16;
      fbmColMix = (rate < 0) ? fbm_colmixrev_565 : fbm_colmix_565;
      break;

    case PSP_DISPLAY_PIXEL_FORMAT_5551:
      fbmPrintSUB = fbm_printSUB16;
      fbmColMix = (rate < 0) ? fbm_colmixrev_5551 : fbm_colmix_5551;
      break;

    case PSP_DISPLAY_PIXEL_FORMAT_4444:
      fbmPrintSUB = fbm_printSUB16;
      fbmColMix = (rate < 0) ? fbm_colmixrev_4444 : fbm_colmix_4444;
      break;

    case PSP_DISPLAY_PIXEL_FORMAT_8888:
      fbmPrintSUB = fbm_printSUB32;
      fbmColMix = (rate < 0) ? fbm_colmixrev_8888 : fbm_colmix_8888;
      break;

    default:
      return -1;
  }

  if (rate < 0) rate = rate * -1 - 1;
  if (rate > 100) rate = 100;

  // utf-8nをUCS2に変換
  utf8_ucs2(str, ucs);
  len = ucslen(ucs);

  for (i = 0; i < len; i++)
  {
    if (ucs[i] == '\n')
    {
      nextx = x;
      nexty += fbmControl[0]->height;
    }
    else
    {
      fbm_get_index(ucs[i], &index, &font_num);
      fbmPrintSUB(vram, bufferwidth, index, font_num, fbmControl[font_num]->height, fbmControl[font_num]->byteperchar / fbmControl[font_num]->height, color, back, fill, rate, mx, my);
    }
  }

  return 0;
}
// TODO Y方向拡大時のループの最適化（１ライン下のデータをキャッシュする様に変更）
#define fbm_printSUB_BODY(depth)                                           \
{                                                                          \
  u16           i;                                                         \
  u16           j;                                                         \
  u16           lx, ly;                                                    \
  u16           shift;                                                     \
  u8            pt;                                                        \
  depth         *vptr;                                                     \
  u32           temp = 0;                                                  \
  fbm_font_t    *font;                                                     \
                                                                           \
  font = fbm_getfont(index, fonttype);                                     \
                                                                           \
  if ((nextx + *(font->width) * mx) > FBM_PSP_WIDTH)                       \
  {                                                                        \
    nextx = 0;                                                             \
    nexty += fbmControl[0]->height * my;                                   \
  }                                                                        \
                                                                           \
  if (nexty + height * my > FBM_PSP_HEIGHT)                                \
  {                                                                        \
    nexty = 0;                                                             \
  }                                                                        \
                                                                           \
  vram = (depth *)vram + nextx + nexty * bufferwidth;                      \
                                                                           \
  for (i = 0; i < height; i++)                                             \
  {                                                                        \
                                                                           \
    for(ly = 0; ly < my; ly++)                                             \
    {                                                                      \
      index = i * byteperline;                                             \
      pt = font->font[index++];                                            \
      vptr = (depth *)vram;                                                \
      shift = 0;                                                           \
                                                                           \
    /* ビットマップの表示 */                                               \
    for (j = 0; j < *(font->width); j++)                                   \
    {                                                                      \
      if (shift >= 8)                                                      \
      {                                                                    \
        shift = 0;                                                         \
        pt = font->font[index++];                                          \
      }                                                                    \
                                                                           \
      if (pt & 0x80)                                                       \
      {                                                                    \
        /* 文字描画時 */                                                   \
        if (fill & 0x01 && rate > 0)                                       \
          temp = color;                                                    \
      }                                                                    \
      else                                                                 \
      {                                                                    \
        /* 背景描画時 */                                                   \
        if (fill & 0x10)                                                   \
          temp = back;                                                     \
      }                                                                    \
                                                                           \
      for(lx = 0; lx < mx; lx++)                                           \
      {                                                                    \
        *vptr = (rate < 100) ? fbmColMix(vptr, temp, rate) : temp;         \
        vptr++;                                                            \
      }                                                                    \
                                                                           \
      shift++;                                                             \
      pt <<= 1;                                                            \
    }                                                                      \
    vram = (depth *)vram + bufferwidth;                                    \
                                                                           \
  }                                                                        \
  }                                                                        \
  nextx = nextx + *(font->width) * mx;                                     \
}                                                                          \

void fbm_printSUB16(void *vram, u16 bufferwidth, u16 index, u16 fonttype, u16 height, u16 byteperline, u32 color, u32 back, u8 fill, s16 rate, u16 mx, u16 my)
fbm_printSUB_BODY(u16);

void fbm_printSUB32(void *vram, u16 bufferwidth, u16 index, u16 fonttype, u16 height, u16 byteperline, u32 color, u32 back, u8 fill, s16 rate, u16 mx, u16 my)
fbm_printSUB_BODY(u32);

#define fbm_colmix_BODY(TYPE, REV, Rm, Gm, Bm, Gs, Bs) \
{                                                      \
  u16 r1, g1, b1;                                      \
  u16 r2, g2, b2;                                      \
                                                       \
  r1 = color & Rm;                                     \
  g1 = (color >> Gs) & Gm;                             \
  b1 = (color >> Bs) & Bm;                             \
                                                       \
  r2 = REV(*(TYPE *)vr) & Rm;                          \
  g2 = REV(*(TYPE *)vr >> Gs) & Gm;                    \
  b2 = REV(*(TYPE *)vr >> Bs) & Bm;                    \
                                                       \
  r1 = ((r1 * rate) + (r2 * (100 - rate)) + 50) / 100; \
  g1 = ((g1 * rate) + (g2 * (100 - rate)) + 50) / 100; \
  b1 = ((b1 * rate) + (b2 * (100 - rate)) + 50) / 100; \
                                                       \
  return r1 | (g1 << Gs) | (b1 << Bs);                 \
}                                                      \

u32 fbm_colmix_565(void *vr, u32 color, s16 rate)
fbm_colmix_BODY(u16, +, 0x1f, 0x3f, 0x1f, 5, 11)

u32 fbm_colmix_5551(void *vr, u32 color, s16 rate)
fbm_colmix_BODY(u16, +, 0x1f, 0x1f, 0x1f, 5, 10)

u32 fbm_colmix_4444(void *vr, u32 color, s16 rate)
fbm_colmix_BODY(u16, +, 0x0f, 0x0f, 0x0f, 4, 8)

u32 fbm_colmix_8888(void *vr, u32 color, s16 rate)
fbm_colmix_BODY(u32, +, 0xff, 0xff, 0xff, 8, 16)

u32 fbm_colmixrev_565(void *vr, u32 color, s16 rate)
fbm_colmix_BODY(u16, ~, 0x1f, 0x3f, 0x1f, 5, 11)

u32 fbm_colmixrev_5551(void *vr, u32 color, s16 rate)
fbm_colmix_BODY(u16, ~, 0x1f, 0x1f, 0x1f, 5, 10)

u32 fbm_colmixrev_4444(void *vr, u32 color, s16 rate)
fbm_colmix_BODY(u16, ~, 0x0f, 0x0f, 0x0f, 4, 8)

u32 fbm_colmixrev_8888(void *vr, u32 color, s16 rate)
fbm_colmix_BODY(u32, ~, 0xff, 0xff, 0xff, 8, 16)

void fbm_get_index(u16 utf8, u16 *index, u16 *font_num)
{
  u16 i;

  *font_num = 0;

  if (use_subfont == 1)
  {
    for (i = 0; i < fbmControl[1]->mapcnt && utf8 >= fbmFontMap[1][i].start; i++)
    {
      if (/*utf8 >= fbmFontMap[1][i].start &&*/ utf8 <= fbmFontMap[1][i].end)
      {
        *font_num = 1;
        *index = utf8 - fbmFontMap[1][i].distance;
        return;
      }
    }
  }
  for (i = 0; i < fbmControl[0]->mapcnt && utf8 >= fbmFontMap[0][i].start; i++)
  {
    if (/*utf8 >= fbmFontMap[0][i].start &&*/ utf8 <= fbmFontMap[0][i].end)
    {
      *index = utf8 - fbmFontMap[0][i].distance;
      return;
    }
  }
  *index = fbmControl[0]->defaultchar;
  return;
}

static const s8* utf8decode(const s8 *utf8, u16 *ucs)
{
  unsigned char c = *utf8++;
  unsigned long code;
  s16 tail = 0;

  if ((c <= 0x7f) || (c >= 0xc2)) {
    /* Start of new character. */
    if (c < 0x80) {        /* U-00000000 - U-0000007F, 1 byte */
      code = c;
    } else if (c < 0xe0) { /* U-00000080 - U-000007FF, 2 bytes */
      tail = 1;
      code = c & 0x1f;
    } else if (c < 0xf0) { /* U-00000800 - U-0000FFFF, 3 bytes */
      tail = 2;
      code = c & 0x0f;
    } else if (c < 0xf5) { /* U-00010000 - U-001FFFFF, 4 bytes */
      tail = 3;
      code = c & 0x07;
    } else {
      /* Invalid size. */
      code = 0xfffd;
    }

    while (tail-- && ((c = *utf8++) != 0)) {
      if ((c & 0xc0) == 0x80) {
        /* Valid continuation character. */
        code = (code << 6) | (c & 0x3f);

      } else {
        /* Invalid continuation char */
        code = 0xfffd;
        utf8--;
        break;
      }
    }
  } else {
    /* Invalid UTF-8 char */
    code = 0xfffd;
  }
  /* currently we don't support chars above U-FFFF */
  *ucs = (code < 0x10000) ? code : 0xfffd;
  return utf8;
}

static u8 utf8_ucs2(const s8 *utf8, u16 *ucs)
{
  while(*utf8 !='\0')
  {
    utf8 = utf8decode(utf8, ucs++);
  }
  *ucs = '\0';
  return 0;
}

static u16 ucslen(const u16 *ucs)
{
  u16 len = 0;
  while(ucs[len] != '\0')
    len++;
  return len;
}
