/*
 * umd.c
 *
 *  Created on: 2009/10/23
 *      Author: takka
 */

#include <pspkernel.h>
#include <stdio.h>
#include <string.h>
#include <pspumd.h>

#include "umd.h"

// UMD
int get_umd_id(char *name)
{
  SceUID umd;
  char buff[UMD_SEC_SIZE];

  umd = sceUmdCheckMedium();
    if(umd == 0)
    {
//        printf("Insert UMD\n");
        umd = sceUmdWaitDriveStat(PSP_UMD_PRESENT);
    }

    umd = sceUmdActivate(1, "disc0:");
//    printf("Mounted disc\n");

    umd = sceUmdWaitDriveStat(PSP_UMD_READY);

  umd = sceIoOpen("umd:", PSP_O_RDONLY, 0777);
  if(umd < 0)
  {
    return -1;
  }

  sceIoLseek(umd, 0x10, SEEK_SET); // 0x8000 0x10セクタを読込み
  sceIoRead(umd, buff, 1);
  sceIoClose(umd);

  memcpy(name, &buff[0x373], 10);       // 0x8373から10byteがUMD ID
  name[10] = '\0';

  return 0;
}
